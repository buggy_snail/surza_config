#include "net_connect.h"

#include <QTimer>


#if defined(Q_OS_LINUX)
#include <unistd.h>
#elif defined(Q_OS_WIN)
#include <process.h>
#endif
#include <cstring>
#include <system_error>


NetConnect::NetConnect(QObject *parent, QHostAddress adr, quint16 port) : QObject(parent)
{

    GetMachineID();
    pid = getpid();

    this->adr = adr;
    this->port = port;

    low_priority_channel=false;


    sock.setSocketOption(QAbstractSocket::LowDelayOption, 1);


    sock_handler = new net_connect_sock(&sock, adr, port, this);
    receiver = new net_connect_receiver(&sock, this);
    sender = new net_connect_sender(&sock, this);

    connect(sender, &net_connect_sender::DropConnection, sock_handler, &net_connect_sock::DropConnection);
    connect(sender, &net_connect_sender::MsgSendResult, this, &NetConnect::MsgSendResult);

    connect(receiver, &net_connect_receiver::DropConnection, sock_handler, &net_connect_sock::DropConnection);
    connect(receiver, &net_connect_receiver::NewMsgReceived, this, &NetConnect::MsgReceived);
    connect(receiver, &net_connect_receiver::NewMsgToSend, this, &NetConnect::NewSpecialMsgNotification);

    connect(&sock, &QTcpSocket::connected, this, &NetConnect::NewConnectionNotification);

}


NetConnect::~NetConnect(){

  sock.disconnectFromHost();

}

void NetConnect::SetLowPriority(bool low)
{
    low_priority_channel = low;
    SendMsg({static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_CHANNEL_PRIORITY), static_cast<uint8_t>(low?1:0), GATE_MSG_PRIORITY::HIGHEST});
}

void NetConnect::Subscribe(GateMessageType type)
{
    subscribe_types.insert(type);
    SendMsg({static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_SUBSCRIBE), static_cast<uint8_t>(type), GATE_MSG_PRIORITY::HIGHEST});
}

void NetConnect::Unsubscribe(GateMessageType type)
{
    subscribe_types.erase(type);
    SendMsg({static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_UNSUBSCRIBE), static_cast<uint8_t>(type), GATE_MSG_PRIORITY::HIGHEST});
}



void NetConnect::GetMachineID(){
   bool f_ok=false;

       std::FILE* f  = std::fopen("/etc/machine-id", "r");
       if(f){
          std::string str;
          int c=EOF, i;
          for(i=0; i<32; i++){
             c=std::fgetc(f);
             if(c==EOF)
                break;
             else
                str+=static_cast<char>(c);
          }
         std::fclose(f);

         if(c!=EOF){
            f_ok=true;

            for(unsigned i=0; i<16; i++)
               machine_id[i] = static_cast<uint8_t>(std::stoul(str.substr(i*2, 2), nullptr, 16));

         }
       }

       if(!f_ok){
          memset(machine_id, 0, 16);
       }

}


void NetConnect::NewSpecialMsgNotification()
{

   net_connect_msg_t msg;

   while(receiver->GetSpecialMessage(msg)){

       if(msg.Type() == static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_LOOPBACK))
           sender->AddMsg(std::move(msg));

   }

}

void NetConnect::NewConnectionNotification()
{
    //connection established

      //send subscribes
    for(auto it : subscribe_types)
        SendMsg({static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_SUBSCRIBE), static_cast<uint8_t>(it), GATE_MSG_PRIORITY::HIGHEST});


      //send channel priority
    SendMsg({static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_CHANNEL_PRIORITY), static_cast<uint8_t>(low_priority_channel?1:0), GATE_MSG_PRIORITY::HIGHEST});

}




bool NetConnect::SendMsg(net_connect_msg_t&& msg){
    if(!msg.CheckMsg())
        return false;

    //move to sender
    return sender->AddMsg(std::move(msg));
}

bool NetConnect::SendMsg(const net_connect_msg_t& msg){
    if(!msg.CheckMsg())
        return false;

    //make copy, move to sender
    //return sender->AddMsg(std::move(net_connect_msg_t{msg}));
    return sender->AddMsg(net_connect_msg_t{msg});

}



bool NetConnect::RecvMsg(net_connect_msg_t& msg){
    return receiver->GetMessage(msg);
}



void NetConnect::Update()
{
    watchdog_msg_t data;
    std::memcpy(data.machine_id, machine_id, 16);
    data.pid = pid;

    SendMsg({static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_WATCHDOG), static_cast<uint8_t>(0), GATE_MSG_PRIORITY::HIGHEST, sizeof(watchdog_msg_t), reinterpret_cast<uint8_t*>(&data)});
}


net_connect_sock::net_connect_sock(QTcpSocket* sock, QHostAddress adr, quint16 port, QObject *parent) : QObject(parent)
{
    this->sock = sock;
    this->adr = adr;
    this->port = port;

    reconnect_timer = new QTimer(this);
    reconnect_timer->setSingleShot(true);
    connect(reconnect_timer, &QTimer::timeout, this, &net_connect_sock::ReconnectTimeout);

    connect(sock, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(SockError(QAbstractSocket::SocketError)));
    connect(sock, &QTcpSocket::disconnected, this, &net_connect_sock::Disconnected);

    reconnect_timer->start(1);
}

bool net_connect_sock::IsConnected()
{
  return (sock->state()==QAbstractSocket::ConnectedState);
}

void net_connect_sock::SockError(QAbstractSocket::SocketError)
{
    reconnect();
}

void net_connect_sock::Disconnected()
{
    reconnect();
}

void net_connect_sock::DropConnection()
{
    reconnect();
}




void net_connect_sock::ReconnectTimeout()
{
    if(sock->state()==QAbstractSocket::SocketState::UnconnectedState)
        sock->connectToHost(adr, port);
    else
        reconnect();
}

void net_connect_sock::reconnect()
{
    if(!reconnect_timer->isActive()){

        if(sock->state()!=QAbstractSocket::SocketState::UnconnectedState)
            sock->disconnectFromHost();
        else
            reconnect_timer->start(NET_CONNECT_RECONNECT_DELAY_MSEC);
    }
}

net_connect_sender::net_connect_sender(QTcpSocket *sock, QObject *parent) : QObject(parent)
{

    this->sock = sock;

    byte_counter = 0;
    bytes_sent = 0;
    data_ptr = nullptr;

    msg_timer = new QTimer(this);
    spec_msg_timer = new QTimer(this);
    check_timeouts_timer = new QTimer(this);

    msg_timer->setSingleShot(true);
    spec_msg_timer->setSingleShot(true);

    connect(msg_timer, &QTimer::timeout, this, &net_connect_sender::MsgTimeout);
    connect(spec_msg_timer, &QTimer::timeout, this, &net_connect_sender::SpecialMsgTimeout);

    check_timeouts_timer->setSingleShot(false);
    check_timeouts_timer->setInterval(1000);
    connect(check_timeouts_timer,& QTimer::timeout, this, &net_connect_sender::TimeoutsCheck);

    connect(sock, &QTcpSocket::connected, this, &net_connect_sender::Connected);
    connect(sock, &QTcpSocket::disconnected, this, &net_connect_sender::Disconnected);
    connect(sock, &QTcpSocket::bytesWritten, this, &net_connect_sender::DataSent);

    connect(this, &net_connect_sender::MsgAdded, this, &net_connect_sender::SendMsg, Qt::QueuedConnection);

    check_timeouts_timer->start();

}


net_connect_sender::~net_connect_sender()
{

}



bool net_connect_sender::AddMsg(net_connect_msg_t&& msg){

    bool res=false;

    queue_mutex.lock();

    try{
        if(msg.Type()<=static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_MAX_NUM)){
            if(special_queue.size()<NET_CONNECT_SEND_QUEUE_LENGTH_MAX){
                special_queue.emplace_back(msg);
                res=true;
            }
        }else{
            if(queue_cnt()<NET_CONNECT_SEND_QUEUE_LENGTH_MAX){
                msg.SetTimestamp();
                queue[static_cast<int>(msg.Priority())].emplace_back(msg);
                res=true;
            }
        }

    }
    catch(...){}

    queue_mutex.unlock();

    if(res)
        emit MsgAdded();

    return res;
}


void net_connect_sender::Disconnected()
{
    spec_msg_timer->stop();

    clear_special_queue();

    state = WR_DISCONNECT;
}

void net_connect_sender::Connected()
{
    spec_msg_timer->stop();

    state = WR_NO_MSG;

    emit MsgAdded();
}



void net_connect_sender::DataSent(qint64 bytes)
{

    if(bytes!=bytes_sent){
        msg_timer->stop();
        spec_msg_timer->stop();
        clear_special_queue();
        state=WR_DISCONNECT;
        emit DropConnection();
    }

    byte_counter-=bytes;
    if(byte_counter==0){
        if(cur_spec_msg.empty()){ //обычное сообщение
            if(!cur_msg.empty()){
                msg_timer->stop();
                emit MsgSendResult(cur_msg.back().Label(), true);
                cur_msg.clear();
            }
        }else{  //специальное
            spec_msg_timer->stop();
            cur_spec_msg.clear();
        }
        state = WR_NO_MSG;
        emit MsgAdded();
    }
    else
        write();

}

void net_connect_sender::SendMsg()
{
    if(state!=WR_NO_MSG)
        return;

    //берем на отправку сообщение - служебное, ранее сохраненное неотправленное, либо новое из очереди на отправку
    if(pop_from_special_queue(tmp_msg))
        cur_spec_msg.push_back(std::move(tmp_msg));
    else if(cur_msg.empty())
             if(pop_from_queue(tmp_msg)){
                 cur_msg.push_back(std::move(tmp_msg));
                 msg_timer->start(NET_CONNECT_MSG_SEND_TIMEOUT_MSEC);  //таймер на сообщение сразу после изъятия из очереди
             }

    //пытаемся отправить
    if(!cur_spec_msg.empty()){
        byte_counter = cur_spec_msg.back().MsgSize();
        data_ptr = cur_spec_msg.back().Msg();
    }else if(!cur_msg.empty()){
        byte_counter = cur_msg.back().MsgSize();
        data_ptr = cur_msg.back().Msg();
    }else return;

    write();
}



void net_connect_sender::TimeoutsCheck()
{
  //проверить все сообщения во входной очереди, удалить с истекшим таймаутом и оповестить испусканием сигнала

      bool res;

      std::vector<uint32_t> labels;

      while(true){

          res = false;

          queue_mutex.lock();

          for(uint8_t pr=0; pr<priority_num; pr++)
              if(!queue[pr].empty()){
                  if(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - queue[pr].front().Timestamp()).count()>NET_CONNECT_MSG_SEND_TIMEOUT_MSEC){
                      labels.push_back(queue[pr].front().Label());
                      queue[pr].pop_front();
                      res = true;
                      break;
                  }
              }

          queue_mutex.unlock();

          if(!res)
              break;

      }

      //удаляю просроченные
      for(auto label : labels)
          emit MsgSendResult(label, false);

}


void net_connect_sender::MsgTimeout()
{
    if(cur_msg.empty())
        return;

    emit MsgSendResult(cur_msg.back().Label(), false);

    cur_msg.clear();

    state = WR_DISCONNECT;
    emit DropConnection();
}

void net_connect_sender::SpecialMsgTimeout()
{

    if(cur_spec_msg.empty())
        return;

    cur_spec_msg.clear();

    state = WR_DISCONNECT;
    emit DropConnection();

}

std::deque<net_connect_msg_t>::size_type net_connect_sender::queue_cnt()
{
    std::deque<net_connect_msg_t>::size_type sum=0;

    for(uint8_t pr=0; pr<priority_num; pr++)
        sum+=queue[pr].size();

    return sum;
}

void net_connect_sender::clear_special_queue()
{
    cur_spec_msg.clear();

    queue_mutex.lock();
    special_queue.clear();
    queue_mutex.unlock();

}

void net_connect_sender::clear_queue()
{
    std::vector<uint32_t> labels;

    queue_mutex.lock();
    for(uint8_t pr=0; pr<priority_num; pr++){
        for(auto& msg: queue[pr])
            labels.push_back(msg.Label());
        queue[pr].clear();
    }
    queue_mutex.unlock();

    for(auto l : labels)
        emit MsgSendResult(l, false);

}


void net_connect_sender::write()
 {
     bytes_sent=sock->write(reinterpret_cast<char*>(data_ptr), static_cast<qint64>(byte_counter));
     if(bytes_sent<0){ //ошибка при отправке, разрываем соединение
         clear_special_queue();
         state=WR_DISCONNECT;
         emit DropConnection();
     }else{
         data_ptr+=bytes_sent;

         if(!cur_spec_msg.empty() && state!=WR_PROCESS)  //таймер на специальное сообщение только если не возникло ошибки сразу  и только при первом вызове write() для этого сообщения
             spec_msg_timer->start(NET_CONNECT_SPEC_MSG_SEND_TIMEOUT_MSEC);

         state=WR_PROCESS;
     }
 }



bool net_connect_sender::pop_from_queue(net_connect_msg_t& msg)
{
    bool res;

    //искать до тех пор , пока не будет найдено не просроченное по времени сообщение
    //все просроченные тут же удалять и извещать сигналом о неотправке
    net_connect_msg_t tmp_msg;

    while(true){

        res = false;

        queue_mutex.lock();

        for(uint8_t pr=0; pr<priority_num; pr++)
            if(!queue[pr].empty()){
                tmp_msg = std::move(queue[pr].front());
                queue[pr].pop_front();
                res = true;
                break;
            }
        queue_mutex.unlock();

        if(!res)
            break;

        if(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - tmp_msg.Timestamp()).count()<NET_CONNECT_MSG_SEND_TIMEOUT_MSEC){
            msg = std::move(tmp_msg);
            break;
        }

        res = false;

        //просрочено
        emit MsgSendResult(tmp_msg.Label(), false);
    }

    return res;
}

bool net_connect_sender::pop_from_special_queue(net_connect_msg_t & msg)
{
    bool res = false;

    queue_mutex.lock();

    if(!special_queue.empty()){
        msg = std::move(special_queue.front());
        special_queue.pop_front();
        res = true;
    }

    queue_mutex.unlock();

    return res;
}



net_connect_receiver::net_connect_receiver(QTcpSocket *sock, QObject *parent) : QObject(parent)
{

    this->sock = sock;

    msg_ptr = nullptr;

    msg_timer = new QTimer(this);
    msg_timer->setSingleShot(true);
    connect(msg_timer, &QTimer::timeout, this, &net_connect_receiver::MsgTimeout);

    state = RD_INITIAL;

    connect(sock, &QTcpSocket::disconnected, this, &net_connect_receiver::Disconnected);
    connect(sock, &QTcpSocket::readyRead, this, &net_connect_receiver::DataAvailable);

}

net_connect_receiver::~net_connect_receiver()
{
    Disconnected();

}


bool net_connect_receiver::GetMessage(net_connect_msg_t& msg)
{
    bool res = false;

    std::lock_guard<std::mutex> lock(recv_queue_mutex);

    for(uint8_t pr=0; pr<priority_num; pr++)
        if(!recv_queue[pr].empty()){
            msg = std::move(recv_queue[pr].front());
            recv_queue[pr].pop_front();
            res = true;
            break;
        }

    return res;
}

bool net_connect_receiver::GetSpecialMessage(net_connect_msg_t& msg)
{
    if(send_queue.empty())
        return false;

    msg = std::move(send_queue.front());
    send_queue.pop_front();

    return true;
}


void net_connect_receiver::Disconnected()
{
    msg_timer->stop();
    state = RD_INITIAL;
}


void net_connect_receiver::DataAvailable()
{
    static uint32_t msg_size;
    qint64 bytes_read;

    bool exit_flag = false;

    while(!exit_flag){
        exit_flag=true;

        switch(state){
        case RD_INITIAL:
            bytes_left = 4;
            data_ptr = reinterpret_cast<char*>(&msg_size);
            state = RD_NO_MSG;
            exit_flag = false;
            break;

        case RD_NO_MSG:
            bytes_read = sock->read(data_ptr, bytes_left);
            if(bytes_read<0){
                msg_timer->stop();
                state = RD_WAIT_DISCONNECT;
                emit DropConnection();
                break;
            }
            if(bytes_read==0)
                break;

            msg_timer->start(NET_CONNECT_MSG_SEND_TIMEOUT_MSEC);

            data_ptr+=bytes_read;
            bytes_left-=bytes_read;

            if(bytes_left==0){
                //msg_size  received

                if(msg_size<=NET_CONNECT_MSG_DATA_LENGTH_MAX && msg_size >= sizeof(net_raw_msg_t) + 4){

                    if(msg.Reset(msg.GetDataSizeFromMsgSize(msg_size))){
                        data_ptr = reinterpret_cast<char*>(msg.Msg());
                        std::memcpy(data_ptr, &msg_size, 4);
                        data_ptr+=4;
                        bytes_left=msg_size-4;
                        exit_flag = false;
                        state = RD_READ_MSG;
                        break;
                    }
                }

                //wrong data or bad alloc
                msg_timer->stop();
                state = RD_WAIT_DISCONNECT;
                emit DropConnection();
            }

            break;

        case RD_READ_MSG:
            bytes_read = sock->read(data_ptr, bytes_left);
            if(bytes_read<0){
                msg_timer->stop();
                state = RD_WAIT_DISCONNECT;
                emit DropConnection();
                break;
            }

            data_ptr+=bytes_read;
            bytes_left-=bytes_read;

            if(bytes_left==0){
                //msg received
                msg_timer->stop();
                if(process_msg()){
                    state = RD_INITIAL;
                    exit_flag = false;
                }else{
                    state = RD_WAIT_DISCONNECT;
                    emit DropConnection();
                }

            }
            break;

        case RD_WAIT_DISCONNECT:
            break;
        }

    }

}


void net_connect_receiver::MsgTimeout()
{
    state = RD_WAIT_DISCONNECT;
    emit DropConnection();
}



bool net_connect_receiver::process_msg()
{

    if(!msg.CheckMsg())
        return false;

    //special message
    if(msg.Type()<=static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_MAX_NUM)){

        if(send_queue.size()<NET_CONNECT_RECV_QUEUE_MAX_LENGTH){
            //move to special queue
            send_queue.push_back(std::move(msg));
            emit NewMsgToSend();
        }

    } else {

        recv_queue_mutex.lock();

        if(recv_queue[static_cast<unsigned>(msg.Priority())].size()<NET_CONNECT_RECV_QUEUE_MAX_LENGTH){
            // move to recv queue
            recv_queue[static_cast<unsigned>(msg.Priority())].push_back(std::move(msg));
            recv_queue_mutex.unlock();

            emit NewMsgReceived();
        }else recv_queue_mutex.unlock();


    }

    return true;
}
