#include "net_task_download_settings_t.h"

#include <cstring>

#include <QStringList>

#include "crc32.h"


net_task_download_settings_t::net_task_download_settings_t(QObject *parent, NetConnect** con) : net_task_base_t(parent, con){

    Subscribe(GateMessageType::SURZA_SETTINGS);

    wait_response_flag = false;
    timer = new QTimer(this);
    timer->setSingleShot(true);

    connect(timer, &QTimer::timeout, [this]{
        wait_response_flag = false;
        log("Ошибка! Нет ответа от контроллера на запрос настроек!");
    });
}

bool net_task_download_settings_t::get_new_settings(QByteArray& hash, std::vector<QString>& strings)
{
    strings.clear();

    if(!data.size())
        return false;

    QString big_str = QString::fromUtf8(data);
    if(!big_str.length())
        return false;

    QStringList str_list = big_str.split('\n', QString::SkipEmptyParts);
    for(const auto& s : str_list)
        strings.push_back(s);

    hash = this->hash;

    return true;
}

void net_task_download_settings_t::download()
{
    data.clear();

    log("Запрос текущих настроек...");


    msg_type_settings_request_t request;
    std::memset(request.md5_hash, 0, sizeof(msg_type_settings_request_t::md5_hash));

    timer->start(5000);
    wait_response_flag=true;

    if(!Send({static_cast<uint8_t>(GateMessageType::SURZA_SETTINGS_REQUEST), 0, GATE_MSG_PRIORITY::MEDIUM, static_cast<unsigned>(sizeof(msg_type_settings_request_t)), reinterpret_cast<uint8_t*>(&request)})){
        log("Ошибка при попытке отправки запроса настроек в сурзу!");
        timer->stop();
    }


}

void net_task_download_settings_t::send_result(__attribute__((unused)) uint32_t label, __attribute__((unused)) bool result)
{
    // !!! убрать атрибут __attribute__((unused)) при реализации

}

void net_task_download_settings_t::new_msg(const net_connect_msg_t &msg)
{
    if(!wait_response_flag)
        return;

    if(msg.Type()!=static_cast<uint8_t>(GateMessageType::SURZA_SETTINGS))
        return;

    wait_response_flag=false;
    timer->stop();

    //проверка сообщения
    bool ok=false;
    msg_type_settings_t* ptr=nullptr;

    while(true){

        if(msg.DataSize() < sizeof(msg_type_settings_t)) break;

        ptr = reinterpret_cast<msg_type_settings_t*>(msg.Data());
        if(!ptr) break;

        if(msg.DataSize() < ptr->data_offset + ptr->bytes) break;

        if(!crc32_check(reinterpret_cast<const char*>(&ptr->crc32)+4, ptr->data_offset + ptr->bytes - 4, ptr->crc32)) break;

        ok = true;
        break;
    }

    if(!ok){
        log("Полученный файл настроек поврежден!");
        return;
    }

    if(ptr->bytes==0){
        log("Файл настроек в контроллере отсутствует!");
        return;
    }

    data.clear();
    data.append(reinterpret_cast<char*>(ptr)+ptr->data_offset, static_cast<int>(ptr->bytes));

    hash.clear();
    hash.append(reinterpret_cast<char*>(&ptr->md5_hash[0]), 16);

    log("Файл настроек получен");

    emit new_settings();

}
