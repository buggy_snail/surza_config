#ifndef TABSURZASETTINGS_H
#define TABSURZASETTINGS_H


#include <vector>

#include <QWidget>
#include <QString>
#include <QTableWidget>

#include "common.h"
#include "param_tree.h"

class TabSurzaSettings : public QWidget
{
    Q_OBJECT
public:
    explicit TabSurzaSettings(QWidget *parent = nullptr, bool access_to_invisible = false);

signals:
    void SetParam(uint16_t num, surza_value_t value);

public slots:

    void SetNewIndi(const indi_type_t* indi){Update(indi);}
    void SetNewConfig(const ParamTreeItem* p);

private:

    struct params_t{
        surza_value_t::TYPE type;
        unsigned index;
        uint16_t param_num;
        QString value_min_str;
        QString value_max_str;
        QString value_default_str;
        surza_value_t min;
        surza_value_t max;
        bool visible;
        QString description;
        surza_value_t current_value;
        surza_value_t new_value;
        params_t(SURZA_TABLE_TYPE type) : min(type), max(type), current_value(type), new_value(type) {this->type = current_value.type;}
    };

    std::vector<params_t> params;

    void ClearWidgets();
    void SetupWidgets();
    void Update(const indi_type_t*);
    bool SetConfig(const ParamTreeItem*);
    void Edit();

    int GetSelectedRow();
    unsigned RowToIndex(int);

    bool  full_access;
    QTableWidget *params_table;

};

#endif // TABSURZASETTINGS_H
