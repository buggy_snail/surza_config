
#include "param_tree.h"


QString ParamTreeItem::spacer = "\t";
QString ParamTreeItem::node_prefix = "$";




bool ParamTreeItem::GetItemValue(const QString& item_name, QString& value) const
{
    for(auto item : items)
        if(!item->IsNode() && item->name==item_name){
            value = item->value;
            return true;
        }
    return false;
}


bool ParamTreeItem::GetItemValue(unsigned item_num, QString &value) const
{
    __attribute__((unused)) QString name;
    return GetItemNameValue(item_num, name, value);
}

bool ParamTreeItem::GetItemNameValue(unsigned item_num, QString& name, QString &value) const
{
    if(item_num>=items.size())
        return false;

    unsigned cnt=0;

    for(auto item : items)
        if(!item->IsNode()){
            if(cnt==item_num){
                value = item->value;
                name = item->name;
                return true;
            }
            cnt++;
        }

    return false;
}

void ParamTreeItem::GetItems(std::vector<std::pair<QString, QString> >& pairs) const
{
    pairs.clear();

    for(auto item : items)
        if(!item->IsNode())
                pairs.push_back({item->name, item->value});

}

void ParamTreeItem::GetItems(std::vector<QString>& values) const
{
   values.clear();

   for(auto item : items)
       if(!item->IsNode())
               values.push_back(item->value);
}

ParamTreeItem* ParamTreeItem::FindNode(const QString& node_name) const
{
    for(auto item : items)
        if(item->IsNode() && item->name==node_name)
            return item;
    return nullptr;
}


void ParamTreeItem::GetStrings(std::vector<QString>& strings, bool add_spacer) const
{
    if(!node) return;

    std::vector<QString> all;
    for(auto p: items)
        if(p){
            all.push_back(p->String());
            if(p->IsNode()){
                std::vector<QString> tmp;
                p->GetStrings(tmp, add_spacer);
                for(auto s : tmp)
                    all.push_back(add_spacer?(spacer + s):s);
                tmp.clear();
                all.push_back(node_prefix+p->String());
            }
        }
    strings = std::move(all);
}

unsigned ParamTreeItem::ItemNum() const
{
    unsigned num=0;
    for(auto item : items)
        if(!item->IsNode())
            num++;
    return num;
}

bool ParamTreeItem::add_item(QString str)
{
    remove_spacer_and_prefix(str);

    int pos = str.indexOf('=');
    if(pos<0)
        return false;

    QString name = str.left(pos);
    pos+=1;
    int n = str.size()-pos;
    QString value = n>0?str.right(n):"";

    AddItem(name, value);

    return true;
}

ParamTreeItem *ParamTreeItem::add_node(QString str)
{
   remove_spacer_and_prefix(str);
   return AddNode(str);
}

void ParamTreeItem::remove_spacer_and_prefix(QString& str)
{
    while(str.compare(str.left(spacer.size()), spacer)==0)
          str.remove(0, spacer.size());

    if(!str.size())
        return;

    if(str.compare(str.left(node_prefix.size()), node_prefix)==0)
          str.remove(0, node_prefix.size());

    if(!str.size())
        return;

    if(str.compare(str.left(node_prefix.size()), node_prefix)==0)
          str.remove(0, node_prefix.size());

}

bool ParamTreeItem::is_node(const QString& str) const
{
    QString::size_type pos=0;

    while(str.compare(str.mid(pos, spacer.size()), spacer)==0)
        pos+=spacer.size();

    return (str[pos]=='$');
}

QString ParamTreeItem::get_pair(const QString& str) const
{
  QString s(str);

  int pos = s.indexOf('$', 0);
  if(pos>=0){
      s.insert(pos, '$');
  }
  return s;
}
