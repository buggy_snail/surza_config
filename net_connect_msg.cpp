
#include "net_connect_msg.h"

#include <cstdlib>
#include <stdexcept>
#include <cstring>
#include <string>

std::atomic<uint32_t> net_connect_msg_t::label_global(static_cast<uint32_t>(std::rand()));



net_connect_msg_t::net_connect_msg_t(uint8_t type, uint8_t client_num, GATE_MSG_PRIORITY priority, unsigned data_size, const uint8_t *data_ptr)
{
    alloc_memory(data_size);

    SetType(type);
    SetClient(client_num);
    SetPriority(priority);
    MakeLabel();

    if(data_ptr)
        std::memcpy(Data(), data_ptr, data_size);

}

net_connect_msg_t::net_connect_msg_t(const net_connect_msg_t& from)
    : net_connect_msg_t(from.Type(),
                        from.Client(),
                        from.Priority(),
                        from.DataSize(),
                        from.Data()){
    SetLabel(from.Label());
    timestamp = from.timestamp;
}

net_connect_msg_t::net_connect_msg_t(net_connect_msg_t&& from)
{
   this->move(this, &from);
}


net_connect_msg_t::~net_connect_msg_t()
{
    dealloc_memory();
}

net_connect_msg_t& net_connect_msg_t::operator=(const net_connect_msg_t& from)
{
    if(this != &from)
       if(Reset(from.DataSize())){
           std::memcpy(raw_data_ptr, from.raw_data_ptr, raw_data_size);
           timestamp = from.timestamp;
       }

    return *this;
}

net_connect_msg_t& net_connect_msg_t::operator=(net_connect_msg_t&& from)
{
    if(this != &from){
       dealloc_memory();
       this->move(this, &from);
    }

    return *this;
}

uint32_t net_connect_msg_t::GetDataSizeFromMsgSize(uint32_t size)
{
    return (size - sizeof(net_raw_msg_t) - 4);
}

uint32_t net_connect_msg_t::GetMsgSizeFromDataSize(uint32_t data_size)
{
    return (sizeof(net_raw_msg_t) + data_size + 4);
}

void net_connect_msg_t::SetType(uint8_t type)
{
    if(!raw_data_ptr) return;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    header->msg.type = type;
}

void net_connect_msg_t::SetClient(uint8_t client_num)
{
    if(!raw_data_ptr) return;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    header->msg.client = client_num;
}

void net_connect_msg_t::SetPriority(GATE_MSG_PRIORITY priority)
{
    if(!raw_data_ptr) return;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    header->priority = static_cast<uint8_t>(priority);
}

uint32_t net_connect_msg_t::MakeLabel(){
    uint32_t label = std::atomic_fetch_add(&label_global, 1u);
    SetLabel(label);
    return Label();
}


void net_connect_msg_t::SetLabel(uint32_t label)
{
    if(!raw_data_ptr) return;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    header->label = label;
    std::memcpy(reinterpret_cast<uint8_t*>((header+1))+header->msg.size, &(header->label), 4);
}

uint32_t net_connect_msg_t::Label() const {
    if(!raw_data_ptr) return 0;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    return header->label;
}


bool net_connect_msg_t::Reset(uint32_t data_size)
{
    dealloc_memory();

    try{alloc_memory(data_size);}
    catch(...){return false;}

    MakeLabel();

    return true;
}


void net_connect_msg_t::alloc_memory(uint32_t data_size)
{
    if(data_size>NET_CONNECT_MSG_DATA_LENGTH_MAX)
        throw std::length_error("Maximum message data length ("+std::to_string(NET_CONNECT_MSG_DATA_LENGTH_MAX)+") exceeded!");

    uint32_t size = this->GetMsgSizeFromDataSize(data_size);

    try{raw_data_ptr = new uint8_t[size];}
    catch(std::bad_alloc){
        raw_data_size=0;
        throw;
    }
    raw_data_size = size;
    SetMsgSize(size);
    SetDataSize(data_size);
}

void net_connect_msg_t::dealloc_memory()
{
    if(raw_data_ptr){
        delete[] raw_data_ptr;
        raw_data_ptr=nullptr;
    }
    raw_data_size = 0;
}


void net_connect_msg_t::move(net_connect_msg_t *dst, net_connect_msg_t *src)
{
    dst->raw_data_ptr = src->raw_data_ptr;
    dst->raw_data_size = src->raw_data_size;
    src->raw_data_ptr = nullptr;
    src->raw_data_size = 0;

    dst->timestamp = src->timestamp;
}

uint8_t *net_connect_msg_t::Data() const
{
    return raw_data_ptr?(raw_data_ptr+sizeof(net_raw_msg_t)):nullptr;
}


void net_connect_msg_t::SetTimestamp(){timestamp = std::chrono::steady_clock::now();}


uint8_t net_connect_msg_t::Type() const
{
    if(!raw_data_ptr) return static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_MAX_NUM);
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    return header->msg.type;
}

uint8_t net_connect_msg_t::Client() const
{
    if(!raw_data_ptr) return 0;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    return header->msg.client;
}

GATE_MSG_PRIORITY net_connect_msg_t::Priority() const
{
    if(!raw_data_ptr) return GATE_MSG_PRIORITY::BACKGROUND;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    return (header->priority > static_cast<uint8_t>(GATE_MSG_PRIORITY::BACKGROUND))?
              GATE_MSG_PRIORITY::BACKGROUND:
              static_cast<GATE_MSG_PRIORITY>(header->priority);
}

uint32_t net_connect_msg_t::DataSize() const
{
    if(!raw_data_ptr) return 0;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    return header->msg.size;
}

void net_connect_msg_t::SetDataSize(uint32_t data_size)
{
    if(!raw_data_ptr) return;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    header->msg.size = data_size;
}


void net_connect_msg_t::SetMsgSize(uint32_t msg_size)
{
    if(!raw_data_ptr) return;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);
    header->size = msg_size;
}


bool net_connect_msg_t::CheckMsg() const
{
    if(!raw_data_ptr) return false;
    net_raw_msg_t* header = reinterpret_cast<net_raw_msg_t*>(raw_data_ptr);

    if(header->size!=raw_data_size)
        return false;

    if(header->size<sizeof(net_raw_msg_t)+4)
        return false;

    if(header->msg.size>NET_CONNECT_MSG_DATA_LENGTH_MAX)
        return false;

    if(header->size!=sizeof(net_raw_msg_t)+header->msg.size+4)
        return false;

    if(std::memcmp(&header->label, raw_data_ptr+(sizeof(net_raw_msg_t)+header->msg.size), 4))
        return false;

    return true;
}

