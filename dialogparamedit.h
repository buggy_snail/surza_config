#ifndef DIALOGPARAMEDIT_H
#define DIALOGPARAMEDIT_H

#include <QDialog>
#include <QString>
#include <QPushButton>
#include <QLineEdit>

#include <common.h>

class DialogParamEdit : public QDialog
{
     Q_OBJECT
public:
    DialogParamEdit(QWidget *parent, QString name, QString Min, QString Max, const surza_value_t& value);
    bool Status(){return status;}
    surza_value_t GetNewValue(){return new_value;}

private:
    bool status;
    surza_value_t new_value;
    QPushButton* button_cancel;
    QPushButton* button_ok;
    QLineEdit *edit;
};

#endif // DIALOGPARAMEDIT_H
