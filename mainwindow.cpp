#include "mainwindow.h"

#include <cstring>

#include <QIcon>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QPushButton>
#include <QApplication>

#include "getipstringdialog.h"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    setWindowTitle("Настройки СУРЗА");
    setWindowIcon(QIcon(":/main_icon.png"));
    setWindowFlags(Qt::Dialog);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    settings_load(settings);


    // инициализация сетевой части
    //======================================================
    con_gate = nullptr;

    // приемники сетевых сообщений
    net_task_download_settings = new net_task_download_settings_t(this, &con_gate);
    msg_receivers.push_back(net_task_download_settings);

    net_task_common = new net_task_common_t(this, &con_gate);
    msg_receivers.push_back(net_task_common);

    GateReconnect();

    connect(net_task_common, &net_task_common_t::new_indi, [this]{
        uint8_t client;
        QByteArray arr;
        while(this->net_task_common->get_new_indi(client, arr))
            this->ParseNewIndi(std::move(arr));
    });

    connect(this, &MainWindow::ip_adr_changed, [this]{this->GateReconnect();});

    connect(this, &MainWindow::RequestNewSettings, net_task_download_settings, &net_task_download_settings_t::download);
    connect(net_task_download_settings, &net_task_download_settings_t::new_settings, [this]{
        std::vector<QString> strings;
        QByteArray hash;
        if(this->net_task_download_settings->get_new_settings(hash, strings))
            this->NewSettings(hash, strings);
        return;
    });
    //======================================================


    //таймер перерисовки
    QTimer* redraw_timer = new QTimer(this);
    redraw_timer->setInterval(1000);



    //настройки связи с сурзой
    QWidget* net_frame = new QWidget(this);
    net_frame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    net_frame->setFixedHeight(40);
    QHBoxLayout* h_layout = new QHBoxLayout(net_frame);
    net_frame->setLayout(h_layout);

    h_layout->setSpacing(18);
    h_layout->addItem(new QSpacerItem(10,10, QSizePolicy::Expanding, QSizePolicy::Expanding));


    QLabel* label_connection = new QLabel(net_frame);
    label_connection->setText(connection_string[0]);
    h_layout->addWidget(label_connection);
    connect(redraw_timer, &QTimer::timeout, this, [this, label_connection](){
        label_connection->setText(connection_string[(con_gate?con_gate->IsConnected():0)?1:0]);});

    QLabel* label_gate_ip = new QLabel(net_frame);
    label_gate_ip->setText("IP адрес СУРЗЫ:");
    h_layout->addWidget(label_gate_ip);

    QLabel* label_gate_ip_ip = new QLabel(net_frame);
    h_layout->addWidget(label_gate_ip_ip);
    auto set_ip_str = [label_gate_ip_ip](QString ip, QString port){
        label_gate_ip_ip->setText(ip + ":" + port);
    };
    connect(this,&MainWindow::ip_adr_changed, this, set_ip_str);
    set_ip_str(settings["surza_ip"], settings["surza_port"]);


    QPushButton* button_change_ip = new QPushButton(net_frame);
    button_change_ip->setText("Изменить IP адрес СУРЗЫ");
    h_layout->addWidget(button_change_ip);
    connect(button_change_ip, &QPushButton::clicked, this, [this, net_frame](){
        GetIPStringDialog dialog(net_frame, settings["surza_ip"], settings["surza_port"]);
        dialog.exec();
        if(!dialog.OK())
            return;

        //проверить отличие новых настроек от предыдущих
        if(   dialog.IP()==settings["surza_ip"]
              && dialog.Port()==settings["surza_port"])
            return;

        //проверить корректность данных
        if(   !check_ip_string(dialog.IP())
              || !check_port_string(dialog.Port())){
            QMessageBox::warning(this, "Настройки СУРЗА", "IP адрес и/или порт заданы некорректно!");
            return;
        }

        //поменять и сохранить настройки
        settings["surza_ip"]=dialog.IP();
        settings["surza_port"]=dialog.Port();

        settings_save(settings);
        emit ip_adr_changed(dialog.IP(), dialog.Port());
    });



    //основная раскладка окна
    this->setCentralWidget(new QWidget());
    this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    this->setFixedSize(800, 600);
    QVBoxLayout* main_layout = new QVBoxLayout();
    this->centralWidget()->setLayout(main_layout);

    main_tab = new QTabWidget(this);
    main_tab->setObjectName(QStringLiteral("TabWidget"));
    main_tab->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


    main_layout->addWidget(main_tab);
    main_layout->addWidget(net_frame);


    //наполенение табами
    bool full_access = (QApplication::arguments().size()>1 && QApplication::arguments().at(1)=="full_access");


    tab_surza_settings = new TabSurzaSettings(main_tab, full_access);
    main_tab->addTab(tab_surza_settings, "Настройки СУРЗА");

    tab_gate_settings = new TabGateSettings(main_tab, full_access);
    main_tab->addTab(tab_gate_settings, "Настройки шлюза");



    for(int i=0; i<main_tab->count(); i++){
        auto w = main_tab->widget(i);
        w->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    }
    main_tab->setCurrentIndex(0);
    //---------------------


    //нацепить события
    connect(this, &MainWindow::ip_adr_changed, tab_gate_settings, &TabGateSettings::reset_all_data);
    connect(this, &MainWindow::NewParams, tab_surza_settings, &TabSurzaSettings::SetNewConfig);
    connect(this, &MainWindow::NewIndi, tab_surza_settings, &TabSurzaSettings::SetNewIndi);
    connect(tab_surza_settings, &TabSurzaSettings::SetParam, [this](uint16_t num, surza_value_t value){
        net_task_common->set_param(settings_hash, num, value);
    });

    connect(tab_gate_settings, &TabGateSettings::gate_settings_msg, net_task_common, &net_task_common_t::send_gate_settings);
    connect(net_task_common, &net_task_common_t::new_gate_settings, tab_gate_settings, &TabGateSettings::new_gate_settings);

    connect(tab_gate_settings, &TabGateSettings::gate_command, net_task_common, &net_task_common_t::send_gate_command);


    redraw_timer->start();

}

MainWindow::~MainWindow()
{

}

void MainWindow::settings_load(MainWindow::settings_t& s)
{
    s = default_settings;

    QDir path;
    path.setPath(QCoreApplication::applicationDirPath());
    QFile file(path.filePath(settings_filename));

    QByteArray arr;

    if(file.open(QIODevice::ReadOnly)){
        arr = file.readAll();
        file.close();
    }

    if(!arr.size())
        return;

    QString big_str(QString::fromUtf8(arr.data(), arr.size()));
    QStringList str_list = big_str.split('\n', QString::SkipEmptyParts);
    std::pair<QString, QString> kv;
    for(const auto& s : str_list){
        if(GetKeyValuePair(s, kv))
            settings[kv.first] = kv.second;
    }

}

void MainWindow::settings_save(const MainWindow::settings_t &)
{
    QString big_str;
    for(const auto&p : settings)
        big_str += p.first + "=" + p.second + '\n';

    QByteArray arr = big_str.toUtf8();

    QDir path;
    path.setPath(QCoreApplication::applicationDirPath());
    QFile file(path.filePath(settings_filename));
    if(file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        file.write(arr.data(), arr.size());
        file.flush();
        file.close();
    }
}

bool MainWindow::GetKeyValuePair(const QString &str, std::pair<QString, QString> &key_value)
{
    int pos = str.indexOf('=');
    if(pos<0)
        return false;

    key_value.first = str.left(pos);
    pos+=1;
    int n = str.size()-pos;
    key_value.second = n>0?str.right(n):"";

    return true;
}

void MainWindow::GateReconnect()
{
    if(con_gate){
        disconnect(con_gate, &NetConnect::MsgReceived, this, &MainWindow::connection_recv);
        disconnect(con_gate, &NetConnect::MsgSendResult, this, &MainWindow::connection_results);
        delete con_gate;
        con_gate = nullptr;
    }

    con_gate = new NetConnect(this,  QHostAddress(settings["surza_ip"]), settings["surza_port"].toUShort());
    if(con_gate){
        connect(con_gate, &NetConnect::MsgReceived, this, &MainWindow::connection_recv);
        connect(con_gate, &NetConnect::MsgSendResult, this, &MainWindow::connection_results);

        for(auto receiver : msg_receivers)
            receiver->new_connection();
    }
}

void MainWindow::connection_recv()
{
    net_connect_msg_t msg;
    while(con_gate->RecvMsg(msg))
        for(auto receiver : msg_receivers)
            receiver->msg(msg);
}


void MainWindow::ParseNewIndi(QByteArray arr)
{


    unsigned size = static_cast<unsigned>(arr.size());

    if(size<sizeof(msg_type_indi_t))
        return;

    msg_type_indi_t* msg = reinterpret_cast<msg_type_indi_t*>(arr.data());

    if(    msg->in_real_offset+msg->in_real_num*4>size
           || msg->in_int_offset+msg->in_int_num*4>size
           || msg->in_bool_offset+msg->in_bool_num>size
           || msg->out_real_offset+msg->out_real_num*4>size
           || msg->out_int_offset+msg->out_int_num*4>size
           || msg->out_bool_offset+msg->out_bool_num>size)
        return;


    //запрос новых настроек
    if(!settings_hash.size()
            || ((std::memcmp(&msg->md5_hash[0], settings_hash.data(), 16))
                && std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - settings_request_timestamp).count()>SETTINGS_REQUEST_RETRY_MSEC))
    {
        settings_request_timestamp = std::chrono::steady_clock::now();
        emit RequestNewSettings();
        return;
    }

    if(indi.size()<6)
        return;

    if(   msg->in_real_num  != indi[0].size()
          || msg->in_int_num   != indi[1].size()
          || msg->in_bool_num  != indi[2].size()
          || msg->out_real_num != indi[3].size()
          || msg->out_int_num  != indi[4].size()
          || msg->out_bool_num != indi[5].size() )
        return;

    unsigned num;

    float* f;
    int32_t* i;
    uint8_t* b;

    for(num=0, f=reinterpret_cast<float*>(reinterpret_cast<uint8_t*>(msg)+msg->in_real_offset); num<msg->in_real_num; num++, f++)
        indi[0][num].value.set(*f);

    for(num=0, i=reinterpret_cast<int32_t*>(reinterpret_cast<uint8_t*>(msg)+msg->in_int_offset); num<msg->in_int_num; num++, i++)
        indi[1][num].value.set(*i);

    for(num=0, b=reinterpret_cast<uint8_t*>(reinterpret_cast<uint8_t*>(msg)+msg->in_bool_offset); num<msg->in_bool_num; num++, b++)
        indi[2][num].value.set((*b)?1:0);

    for(num=0, f=reinterpret_cast<float*>(reinterpret_cast<uint8_t*>(msg)+msg->out_real_offset); num<msg->out_real_num; num++, f++)
        indi[3][num].value.set(*f);

    for(num=0, i=reinterpret_cast<int32_t*>(reinterpret_cast<uint8_t*>(msg)+msg->out_int_offset); num<msg->out_int_num; num++, i++)
        indi[4][num].value.set(*i);

    for(num=0, b=reinterpret_cast<uint8_t*>(reinterpret_cast<uint8_t*>(msg)+msg->out_bool_offset); num<msg->out_bool_num; num++, b++)
        indi[5][num].value.set((*b)?1:0);


    emit NewIndi(&indi);

}

void MainWindow::NewSettings(const QByteArray &hash, std::vector<QString> &strings)
{
    if(settings_hash.size()){  //если ранее уже были настройки
        if(std::memcmp(hash.data(), settings_hash.data(), 16))  //если хеш отличается от текущего
            emit WrongParams();
        else //если одинаковый, то делать ничего не надо
            return;
    }

    ParamTree.Clear();

    if(ParamTree.AddStrings(strings.begin(), strings.end())){
        settings_hash = hash;
        SetupIndi(ParamTree);
        emit NewParams(&ParamTree);
    }
}

void MainWindow::SetupIndi(ParamTreeItem& params)
{
    indi.clear();

    //заполнение индикаторов
    ParamTreeItem* main_node = params.FindNode("MAIN_TABLES");
    if(!main_node)
        return;

    ParamTreeItem* strings_node = params.FindNode("STRINGS");
    if(!strings_node)
        return;

    std::vector<std::pair<QString, QString>> items;

    ParamTreeItem* node;
    QString str;

    //заполняю все таблицы индикаторов
    for(unsigned type_num=0; type_num<6; type_num++){
        indi.push_back(std::vector<indi_t>());
        node = main_node->FindNode(indi_t::MAIN_TABLE_NAME[type_num]);
        if(node){
            node->GetItems(items);
            for(auto& item : items){
                str.clear();
                strings_node->GetItemValue(item.second, str);
                indi[type_num].push_back({{static_cast<SURZA_TABLE_TYPE>(type_num)}, "", item.first, str});
            }
        }
    }

}
