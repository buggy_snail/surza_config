#ifndef GETIPSTRINGDIALOG_H
#define GETIPSTRINGDIALOG_H

#include <QDialog>
#include <QString>
#include <QLineEdit>
#include <QPushButton>

class GetIPStringDialog : public QDialog
{
    Q_OBJECT
public:
    explicit GetIPStringDialog(QWidget *parent, const QString& ip, const QString& port);
    QString IP(){return str_ip;}
    QString Port(){return str_port;}
    bool OK(){return ok_button;}

private:

    bool ok_button;

    QString str_ip;
    QString str_port;
    bool n_gate_mode;
    QLineEdit *edit_ip, *edit_port;
    QPushButton* button;

};

#endif // GETIPSTRINGDIALOG_H
