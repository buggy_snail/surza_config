#include "tabgatesettings.h"

#include <QGridLayout>
#include <QLabel>
#include <QSpacerItem>

#include "net_messages.h"
#include "common.h"

TabGateSettings::TabGateSettings(QWidget *parent, bool full_access) : QWidget(parent)
{

    this->full_access = full_access;

    timer = new QTimer(this);
    timer->setInterval(2000);
    connect(timer, &QTimer::timeout, this, &TabGateSettings::StartRequest);

    CreateWidgets();

    StartRequest();
}

void TabGateSettings::reset_all_data(){
    CleanWidgets();
    StartRequest();
}

void TabGateSettings::new_gate_settings(QByteArray data)
{

    if(!data.length())
        return;
    unsigned total_len = static_cast<unsigned>(data.length());

    if(total_len<sizeof(msg_type_gate_settings_t))
        return;

    msg_type_gate_settings_t* msg = reinterpret_cast<msg_type_gate_settings_t*>(data.data());

    unsigned s_num = msg->n_of_settings;
    if(s_num>0xffff)
        return;

    if(msg->payload_offset+msg->payload_size>total_len)
        return;

    if(msg->is_request)
        return;

    total_len = msg->payload_size;

    uint8_t* ptr = reinterpret_cast<uint8_t*>(msg) + msg->payload_offset;

    while(s_num--){

        gate_setting_t* s_ptr = reinterpret_cast<gate_setting_t*>(ptr);
        if(total_len<sizeof(gate_setting_t)
           || total_len<s_ptr->size)
            break;

        if(s_ptr->size<s_ptr->payload_offset+s_ptr->payload_size)
            break;

        set_settings(s_ptr->type, QByteArray(reinterpret_cast<const char*>(ptr)+s_ptr->payload_offset, static_cast<int>(s_ptr->payload_size)));

        ptr += s_ptr->size;
        total_len -= s_ptr->size;
    }



#if 0
    timer->stop();

    //проверить data

    //.......

    SetWidgets();
#endif
}

void TabGateSettings::StartRequest()
{

    // запрос новых настроек шлюза

    //emit void gate_command(...);

    timer->start(2000);

}

void TabGateSettings::CreateWidgets()
{

    QGridLayout* layout = new QGridLayout(this);



    edit_ipA = new QLineEdit(this);
    edit_ipA->setAlignment(Qt::AlignCenter);
    edit_ipA->setText("");
    edit_ipA->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    edit_ipA->setFixedWidth(120);

    edit_maskA = new QLineEdit(this);
    edit_maskA->setAlignment(Qt::AlignCenter);
    edit_maskA->setText("");
    edit_maskA->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    edit_maskA->setFixedWidth(120);

    button_set_ip_A = new QPushButton("Изменить IP-адрес");
    button_set_ip_A->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    //-----------------------------

    edit_ipB = new QLineEdit(this);
    edit_ipB->setAlignment(Qt::AlignCenter);
    edit_ipB->setText("");
    edit_ipB->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    edit_ipB->setFixedWidth(120);

    edit_maskB = new QLineEdit(this);
    edit_maskB->setAlignment(Qt::AlignCenter);
    edit_maskB->setText("");
    edit_maskB->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    edit_maskB->setFixedWidth(120);

    button_set_ip_B = new QPushButton("Изменить IP-адрес");
    button_set_ip_B->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    //-------------------------------

    if(full_access){
        edit_ipC = new QLineEdit(this);
        edit_ipC->setAlignment(Qt::AlignCenter);
        edit_ipC->setText("");
        edit_ipC->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        edit_ipC->setFixedWidth(120);

        edit_maskC = new QLineEdit(this);
        edit_maskC->setAlignment(Qt::AlignCenter);
        edit_maskC->setText("");
        edit_maskC->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        edit_maskC->setFixedWidth(120);

        button_set_ip_C = new QPushButton("Изменить IP-адрес");
        button_set_ip_C->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    }else{
        edit_ipC = nullptr;
        edit_maskC = nullptr;
        button_set_ip_C = nullptr;
    }

    //---------------------------------


    edit_NTP1 = new QLineEdit(this);
    edit_NTP1->setAlignment(Qt::AlignCenter);
    edit_NTP1->setText("");
    edit_NTP1->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    edit_NTP1->setFixedWidth(120);

    edit_NTP2 = new QLineEdit(this);
    edit_NTP2->setAlignment(Qt::AlignCenter);
    edit_NTP2->setText("");
    edit_NTP2->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    edit_NTP2->setFixedWidth(120);

    button_set_ntp = new QPushButton("Изменить IP-адреса серверов NTP");

    int row=0;

    layout->addWidget(new QLabel("IP-адрес и маска для порта A:"), row, 0, 1, 1);
    layout->addWidget(edit_ipA, row, 1, 1, 1);
    layout->addWidget(edit_maskA, row, 2, 1, 1);
    layout->addWidget(button_set_ip_A, row, 3, 1, 1);
    row++;
    layout->addWidget(new QLabel("IP-адрес и маска для порта B:"), row, 0, 1, 1);
    layout->addWidget(edit_ipB, row, 1, 1, 1);
    layout->addWidget(edit_maskB, row, 2, 1, 1);
    layout->addWidget(button_set_ip_B, row, 3, 1, 1);
    row++;
    if(full_access){
        layout->addWidget(new QLabel("IP-адрес и маска для порта C:"), row, 0, 1, 1);
        layout->addWidget(edit_ipC, row, 1, 1, 1);
        layout->addWidget(edit_maskC, row, 2, 1, 1);
        layout->addWidget(button_set_ip_C, row, 3, 1, 1);
        row++;
    }
    layout->addWidget(new QLabel("IP-адреса NTP серверов:"), row, 0, 1, 1);
    layout->addWidget(edit_NTP1, row, 1, 1, 1);
    layout->addWidget(edit_NTP2, row, 2, 1, 1);
    layout->addWidget(button_set_ntp, row, 3, 1, 1);
    row++;


    if(full_access){
        button_reduce_journal = new QPushButton("Очистить базу данных от старых конфигураций");
        layout->addWidget(button_reduce_journal, row++, 3, 2, 1);
    }else
        button_reduce_journal = nullptr;



    const int last_column = 3;

    layout->addItem(new QSpacerItem(1, 1 , QSizePolicy::Minimum, QSizePolicy::Expanding), row++, 0, 1, last_column);
    layout->addItem(new QSpacerItem(1, 1 , QSizePolicy::Expanding, QSizePolicy::Minimum), 0, last_column+1, 1, row);


    button_request_all = new QPushButton("Считать все настройки");
    button_request_all->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    button_send_all = new QPushButton("Изменить все настройки");
    button_send_all->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    layout->addWidget(button_request_all, row, 0, 1, 2);
    layout->addWidget(button_send_all, row, 2, 1, 2);
    row++;

    this->setLayout(layout);


    iface_name.emplace_back("A");
    iface_name.emplace_back("B");
    iface_name.emplace_back("C");

    //навесить колбеки
    connect(button_send_all, &QPushButton::clicked, this, &TabGateSettings::send_all);
    connect(button_request_all, &QPushButton::clicked, this, &TabGateSettings::request_all);

    connect(button_set_ntp, &QPushButton::clicked, this, &TabGateSettings::send_ntp);

    connect(button_set_ip_A, &QPushButton::clicked, [this](){send_ip(0);});
    connect(button_set_ip_B, &QPushButton::clicked, [this](){send_ip(1);});
    if(button_set_ip_C)
        connect(button_set_ip_C, &QPushButton::clicked, [this](){send_ip(2);});

    if(button_reduce_journal)
        connect(button_reduce_journal, &QPushButton::clicked, [this](){emit gate_command(GATE_COMMAND_DB_REDUCE_CONFIGS, QByteArray());});


}

void TabGateSettings::CleanWidgets()
{
    edit_ipA->clear();
    edit_ipB->clear();
    if(edit_ipC)
        edit_ipC->clear();
    edit_maskA->clear();
    edit_maskB->clear();
    if(edit_maskC)
        edit_maskC->clear();
    edit_NTP1->clear();
    edit_NTP2->clear();
}

void TabGateSettings::send_ip(unsigned iface_n)
{
    QByteArray arr, set_arr;
    SettingsMsgNew(arr, false);

    if(!get_ip(iface_n, set_arr))
        return;

    unsigned s_num=0;
    switch(iface_n){
    case 0: s_num = GATE_SETTING_IP_A; break;
    case 1: s_num = GATE_SETTING_IP_B; break;
    case 2: s_num = GATE_SETTING_IP_C; break;
    default: return;
    }
    SettingsMsgAdd(arr, s_num, set_arr);
    emit gate_settings_msg(arr);
}

void TabGateSettings::send_ntp()
{
    QByteArray arr, set_arr;
    SettingsMsgNew(arr, false);

    if(!get_ntp(0, set_arr)) return;
    SettingsMsgAdd(arr, GATE_SETTING_NTP_1, set_arr);

    if(!get_ntp(1, set_arr)) return;
    SettingsMsgAdd(arr, GATE_SETTING_NTP_2, set_arr);

    emit gate_settings_msg(arr);
}

void TabGateSettings::send_all()
{
    QByteArray arr, set_arr;
    SettingsMsgNew(arr, false);

    if(!get_ip(0, set_arr)) return;
    SettingsMsgAdd(arr, GATE_SETTING_IP_A, set_arr);

    if(!get_ip(1, set_arr)) return;
    SettingsMsgAdd(arr, GATE_SETTING_IP_B, set_arr);

    if(full_access){
        if(!get_ip(2, set_arr)) return;
        SettingsMsgAdd(arr, GATE_SETTING_IP_C, set_arr);
    }

    if(!get_ntp(0, set_arr)) return;
    SettingsMsgAdd(arr, GATE_SETTING_NTP_1, set_arr);

    if(!get_ntp(1, set_arr)) return;
    SettingsMsgAdd(arr, GATE_SETTING_NTP_2, set_arr);

    emit gate_settings_msg(arr);
}

void TabGateSettings::request_all()
{
   QByteArray arr, set_arr;
   SettingsMsgNew(arr, true);

   SettingsMsgAdd(arr, GATE_SETTING_IP_A);
   SettingsMsgAdd(arr, GATE_SETTING_IP_B);
   if(full_access)
       SettingsMsgAdd(arr, GATE_SETTING_IP_C);

   SettingsMsgAdd(arr, GATE_SETTING_NTP_1);
   SettingsMsgAdd(arr, GATE_SETTING_NTP_2);

   emit gate_settings_msg(arr);
}

void TabGateSettings::SettingsMsgNew(QByteArray &msg, bool is_request)
{
    msg.resize(sizeof(msg_type_gate_settings_t));
    auto ptr = reinterpret_cast<msg_type_gate_settings_t*>(msg.data());
    ptr->n_of_settings = 0;
    ptr->is_request = is_request?1:0;
    ptr->payload_size = 0;
    ptr->payload_offset = sizeof(gate_setting_t);
}

void TabGateSettings::SettingsMsgAdd(QByteArray &msg, unsigned settings_id, const QByteArray& data)
{
    const auto s_size = sizeof(gate_setting_t);
    int old_size = msg.size();

    //сначала добавляю только заголовок
    msg.resize(msg.size()+static_cast<int>(s_size));
     //указатели беру обязательно только после ресайза!!
    auto ptr = reinterpret_cast<msg_type_gate_settings_t*>(msg.data());
    auto setting_ptr = reinterpret_cast<gate_setting_t*>(msg.data()+old_size);
    ptr->payload_size += s_size;
    ptr->n_of_settings++;

    setting_ptr->type = settings_id;
    setting_ptr->size = s_size;
    setting_ptr->payload_offset = s_size;
    setting_ptr->payload_size = 0;
    if(ptr->is_request)
        return;

    //если передача самой настройки, то добавляю и данные

     // сначала обязательно обновить поля структур, так как после .append() они протухнут
    const unsigned add_size = static_cast<unsigned>(data.size());
    setting_ptr->size += add_size;
    setting_ptr->payload_size = add_size;
    ptr->payload_size += add_size;

    // добавляю сами данные
    msg.append(data);

    return;
}

bool TabGateSettings::set_settings(unsigned type, QByteArray data)
{
    switch(type){
      case GATE_SETTING_IP_A:  if(data.length()<static_cast<int>(sizeof(gate_setting_ip))) return false;
                               return set_ip(0, data);
      case GATE_SETTING_IP_B:  if(data.length()<static_cast<int>(sizeof(gate_setting_ip))) return false;
                               return set_ip(1, data);
      case GATE_SETTING_IP_C:  if(data.length()<static_cast<int>(sizeof(gate_setting_ip))) return false;
                               return set_ip(2, data);
      case GATE_SETTING_NTP_1: if(data.length()<static_cast<int>(sizeof(gate_setting_ntp))) return false;
                               return set_ntp(0, data);
      case GATE_SETTING_NTP_2: if(data.length()<static_cast<int>(sizeof(gate_setting_ntp))) return false;
                               return set_ntp(1, data);
    default: break;
    }

    return true;
}

bool TabGateSettings::get_ip(unsigned num, QByteArray &data)
{
    QLineEdit *ip, *mask;
    get_ip_mask_widgets(num, &ip, &mask);

    if(!ip || !mask)
        return false;

    data.resize(sizeof(gate_setting_ip));
    gate_setting_ip* msg = reinterpret_cast<gate_setting_ip*>(data.data());

    if(!check_ip_string(ip->text(), msg->ip)){
        show_msg_box("IP адрес интерфейса "+iface_name[num] + " задан некорректно!");
        return false;
    }
    if(!check_mask_string(mask->text(), msg->mask)){
        show_msg_box("Маска интерфейса "+iface_name[num] + " задана некорректно!");
        return false;
    }

    return true;
}

bool TabGateSettings::set_ip(unsigned num, QByteArray &data)
{

    QLineEdit *ip, *mask;

    get_ip_mask_widgets(num, &ip, &mask);

    if(!ip || !mask)
        return false;


    //вывести на экран IP адрес и маску
    if(data.size()<static_cast<int>(sizeof(gate_setting_ip)))
        return false;


    gate_setting_ip* msg = reinterpret_cast<gate_setting_ip*>(data.data());
    ip->setText(QString::number(msg->ip[0])+QString(".")
               +QString::number(msg->ip[1])+QString(".")
               +QString::number(msg->ip[2])+QString(".")
               +QString::number(msg->ip[3]));

    mask->setText(QString::number(msg->mask[0])+QString(".")
                 +QString::number(msg->mask[1])+QString(".")
                 +QString::number(msg->mask[2])+QString(".")
                 +QString::number(msg->mask[3]));

    return true;
}

bool TabGateSettings::get_ntp(unsigned num, QByteArray &data)
{
    QLineEdit *ip;
    get_ntp_widget(num, &ip);

    if(!ip)
        return false;

    data.resize(sizeof(gate_setting_ntp));
    gate_setting_ntp* msg = reinterpret_cast<gate_setting_ntp*>(data.data());

    if(!check_ip_string(ip->text(), msg->ip)){
        show_msg_box("IP адрес NTP сервера #" + QString::number(num+1) + " задан некорректно!");
        return false;
    }

    return true;
}

bool TabGateSettings::set_ntp(unsigned num, QByteArray &data)
{
    QLineEdit *ip;

    get_ntp_widget(num, &ip);

    if(!ip)
        return false;

    //вывести на экран IP адрес NTP сервера

    if(data.size()<static_cast<int>(sizeof(gate_setting_ntp)))
        return false;

    gate_setting_ntp* msg = reinterpret_cast<gate_setting_ntp*>(data.data());
    ip->setText(QString::number(msg->ip[0])+QString(".")
               +QString::number(msg->ip[1])+QString(".")
               +QString::number(msg->ip[2])+QString(".")
               +QString::number(msg->ip[3]));

    return true;
}


#if 0
void TabGateSettings::setup_ip()
{
    //считать новый ip

    //.......


    //emit gate_command(...);

}

void TabGateSettings::setup_ntp()
{
    //считать новые адреса ntp

    //.......




    //emit gate_command(...);

    CleanWidgets();
    StartRequest();

}
#endif
