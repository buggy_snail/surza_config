#ifndef COMMON_H
#define COMMON_H

#include <QString>
#include <QMessageBox>

#include <cstdint>
#include <vector>


void show_msg_box(const QString&, QMessageBox::Icon=QMessageBox::Icon::Warning);

bool check_ip_string(const QString& str, uint32_t* ip=nullptr);
bool check_port_string(const QString& str , uint16_t* port=nullptr);

bool check_ip_string(const QString& str,  uint8_t ip[]);
bool check_mask_string(const QString& str,  uint8_t mask[]);



enum SURZA_TABLE_TYPE {
    TABLE_IN_REAL=0,
    TABLE_IN_INT,
    TABLE_IN_BOOL,
    TABLE_OUT_REAL,
    TABLE_OUT_INT,
    TABLE_OUT_BOOL
};


#pragma pack(1)

struct surza_value_t{
    enum TYPE {
        FLOAT=0,
        INT32,
        BOOL
    } type;
    static const QString TYPE_NAMES[3];
    union{
        float f32;
        int32_t i32;
    } value;
    surza_value_t(){type=FLOAT; value.f32=0.f;}
    surza_value_t(TYPE t){type=t; value.f32=0.f;}
    surza_value_t(SURZA_TABLE_TYPE t){switch(t){
                                       case TABLE_IN_REAL: case TABLE_OUT_REAL: type = FLOAT; break;
                                       case TABLE_IN_INT: case TABLE_OUT_INT: type = INT32; break;
                                       case TABLE_IN_BOOL: case TABLE_OUT_BOOL: type = BOOL; break;
                                      }
                                      value.f32=0.f;}

    template <class T>
    void set(T val){ if(type==FLOAT) value.f32 = static_cast<float>(val);
                     else value.i32 = static_cast<int32_t>(val); }

    template <class T>
    T get() const {return static_cast<T>( type==FLOAT? value.f32 : value.i32);}

    QString print() const {return ( type==FLOAT ? QString::number(static_cast<double>(value.f32)) : QString::number(value.i32) );}
    bool Set(const QString& s){
        bool ok;
        switch(type){
         case FLOAT: value.f32 = s.toFloat(&ok); break;
         case INT32: value.i32 = s.toInt(&ok); break;
         case BOOL: value.i32 = (s.toUInt(&ok)==0?0:1); break;}
        return ok;
    }
    bool operator>(const surza_value_t& right){if(type!=right.type) return false; return (type==FLOAT)?(value.f32>right.value.f32):(value.i32>right.value.i32);}
    bool operator>=(const surza_value_t& right){if(type!=right.type) return false; return (type==FLOAT)?(value.f32>=right.value.f32):(value.i32>=right.value.i32);}
    bool operator<(const surza_value_t& right){if(type!=right.type) return false; return (type==FLOAT)?(value.f32<right.value.f32):(value.i32<right.value.i32);}
    bool operator<=(const surza_value_t& right){if(type!=right.type) return false; return (type==FLOAT)?(value.f32<=right.value.f32):(value.i32<=right.value.i32);}
    bool operator==(const surza_value_t& right){if(type!=right.type) return false; return (value.i32!=right.value.i32);}
    bool operator!=(const surza_value_t& right){if(type!=right.type) return true; return (value.i32==right.value.i32);}

} __attribute__((packed));

#pragma pack()




struct indi_t{
    static const QString MAIN_TABLE_NAME[6];
    surza_value_t value;
    QString value_str;
    QString name;
    QString description;
};

typedef std::vector<std::vector<indi_t>> indi_type_t;




#endif
