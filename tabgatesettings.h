#ifndef TABGATESETTINGS_H
#define TABGATESETTINGS_H

#include <QWidget>
#include <QByteArray>
#include <QTimer>
#include <QPushButton>
#include <QLineEdit>

class TabGateSettings : public QWidget
{
    Q_OBJECT
public:
    explicit TabGateSettings(QWidget *parent, bool full_access = false);

signals:
    //команда для шлюза
    void gate_command(unsigned, QByteArray);
    void gate_settings_msg(QByteArray data);

public slots:

    //сброс отображения настроек
    void reset_all_data();
    //получение новых настроек
    void new_gate_settings(QByteArray data);

private:

    bool full_access;

    void StartRequest();
    void CreateWidgets();
    void CleanWidgets();
    void SetWidgets(){}

    QTimer* timer;
    QPushButton* button_set_ip_A;
    QPushButton* button_set_ip_B;
    QPushButton* button_set_ip_C;
    QPushButton* button_set_ntp;
    QPushButton* button_reduce_journal;

    QPushButton* button_request_all;
    QPushButton* button_send_all;

    QLineEdit *edit_ipA;
    QLineEdit *edit_ipB;
    QLineEdit *edit_ipC;
    QLineEdit *edit_maskA;
    QLineEdit *edit_maskB;
    QLineEdit *edit_maskC;

    void get_ip_mask_widgets(unsigned num, QLineEdit** ip, QLineEdit** mask){
        switch(num){
        case 0:  *ip = edit_ipA; *mask = edit_maskA; break;
        case 1:  *ip = edit_ipB; *mask = edit_maskB; break;
        case 2:  *ip = edit_ipC; *mask = edit_maskC; break;
        default: *ip=nullptr; *mask=nullptr; break;}
    }
    void get_ntp_widget(unsigned num, QLineEdit** ip){
        switch(num){
        case 0:  *ip = edit_NTP1; break;
        case 1:  *ip = edit_NTP2; break;
        default: *ip=nullptr;}
    }

    QLineEdit *edit_NTP1;
    QLineEdit *edit_NTP2;

    std::vector<QString> iface_name;

    void send_ip(unsigned iface_n);
    void send_ntp();

    void send_all();
    void request_all();

    void SettingsMsgNew(QByteArray& msg, bool is_request);
    void SettingsMsgAdd(QByteArray& msg, unsigned settings_id, const QByteArray& data = QByteArray());

    bool set_settings(unsigned type, QByteArray data);

    //получение настроек
    bool get_ip(unsigned num, QByteArray& data);
    bool set_ip(unsigned num, QByteArray& data);

    bool get_ntp(unsigned num, QByteArray& data);
    bool set_ntp(unsigned num, QByteArray& data);





};

#endif // TABGATESETTINGS_H
