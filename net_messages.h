#ifndef NET_MESSAGES_H
#define NET_MESSAGES_H


#include <stdint.h>


#pragma pack(1)


struct net_msg_t{
    uint8_t  type;           //message type
    uint8_t  client;         //client num
    uint32_t size;           //data[] length in bytes
    //uint8_t  data[];       //message data
    //uint32_t label;        //copy of label net_raw_msg_t::label
} __attribute__((packed));


struct net_raw_msg_t{
    uint32_t size;         //message size (net_raw_msg_t + net_msg_t)
    uint32_t label;        //message label
    uint8_t  priority;     //message priority (GateConnect::PRIORITY)
    net_msg_t msg;         //message
} __attribute__((packed));



//watchdog message
struct watchdog_msg_t {
   uint8_t machine_id[16];
   int32_t pid;
}__attribute__((packed));





struct surza_time_t{
    int64_t  secs;   //seconds (unix time)
    uint32_t nsecs;  //nanoseconds
    uint64_t steady_nsecs;  //steady clock nsecs
} __attribute__((packed));



//message types
enum class GateMessageType : uint8_t{
    GATE_SPECIAL_MSG_TYPE_LOOPBACK=0,
    GATE_SPECIAL_MSG_TYPE_WATCHDOG,
    GATE_SPECIAL_MSG_TYPE_SUBSCRIBE,
    GATE_SPECIAL_MSG_TYPE_UNSUBSCRIBE,
    GATE_SPECIAL_MSG_TYPE_CHANNEL_PRIORITY,
    //add new special types here

    GATE_SPECIAL_MSG_TYPE_MAX_NUM=9,

    //application message types
    SURZA_FIRMWARE,
    SURZA_SETTINGS,
    SURZA_SETTINGS_REQUEST,
    SURZA_INDI,
    SURZA_SET_PARAM,
    SURZA_JOURNAL_EVENT,
    SURZA_JOURNAL_INFO,
    SURZA_JOURNAL_REQUEST, 
    SURZA_OSCILLOSCOPE,
    SURZA_SET_INPUT,
    GATE_IEC60870_DATA,
    GATE_IEC60870_EVENTS,
    SURZA_COMMAND,
    GATE_COMMAND,
    GATE_SETTINGS,

    VYBORG_INFO=40
};


//message priority
enum class GATE_MSG_PRIORITY : uint8_t {
    HIGHEST = 0,
    HIGH,
    MEDIUM,
    LOW,
    LOWEST,
    BACKGROUND
};



//type 0
struct TYPE0_msg_t{
   uint16_t some_data[10];
}__attribute__((packed));


//firmware
struct msg_type_firmware_t{
   uint32_t  crc32;         // crc32 for rest of struct
   uint32_t  data_offset;   // (.firmware[] offset in bytes from start of msg_type_firmware_t struct) >>> for upward compatibility purpose <<<
   uint32_t  bytes;
   //dummy bytes possible []
   //uint8_t[bytes] firmware;
}__attribute__((packed));


//settings
struct msg_type_settings_t{
   uint32_t  crc32;      // crc32 for rest of struct
   uint32_t  data_offset;   // (.settings[] offset in bytes from start of msg_type_settings_t struct) >>> for upward compatibility purpose <<<
   uint32_t  bytes;
   uint8_t   md5_hash[16];
   //dummy bytes possible []
   //uint8_t[bytes] data;
}__attribute__((packed));

//settings request
struct msg_type_settings_request_t{
   uint8_t   md5_hash[16];    //request settings with hash 'md5_hash', or current settings in case md5_hash contains all zeros
}__attribute__((packed));



//indi
struct msg_type_indi_t {
    uint32_t   header_size;     //sizeof(msg_type_settings_request_t) (for upward compatibility)
    uint8_t    md5_hash[16];    //current settings hash
    uint32_t   in_real_num;
    uint32_t   in_real_offset;
    uint32_t   in_int_num;
    uint32_t   in_int_offset;
    uint32_t   in_bool_num;
    uint32_t   in_bool_offset;
    uint32_t   out_real_num;
    uint32_t   out_real_offset;
    uint32_t   out_int_num;
    uint32_t   out_int_offset;
    uint32_t   out_bool_num;
    uint32_t   out_bool_offset;
    surza_time_t time;
    uint64_t   launchnum;
    // add additional fields here

} __attribute__((packed));


// set parameter
struct msg_type_set_param_t {
    uint8_t hash[16]; //config id
    uint16_t num;   // param num (0-0xfffe),    0xffff - set all to default
    union{
        float f32;
        int32_t i32;
    } value;
} __attribute__((packed));


struct msg_type_vyborg_info_t{
   uint32_t  header[10];
   uint16_t  regs[100];
} __attribute__((packed));



// journal messages

// journal events
struct msg_type_journal_event_t{
    uint8_t  md5_hash[16];
    uint64_t unique_id;
    surza_time_t time;
    uint32_t n_of_events;
    uint32_t events_offset;
    uint32_t n_of_data_real;   //events data of type REAL
    uint32_t data_real_offset; //REAL data offset
    uint32_t n_of_data_int;
    uint32_t data_int_offset;
    uint32_t n_of_data_bool;
    uint32_t data_bool_offset;
    //add new fields here
} __attribute__((packed));

// journal info
struct msg_type_journal_info_t {
    uint8_t  md5_hash[16];
    uint32_t events_num;
    uint64_t head_id;
    uint64_t tail_id;
} __attribute__((packed));

#define  MSG_JOURNAL_REQUEST_GET     0
#define  MSG_JOURNAL_REQUEST_DELETE  1

// journal request
struct msg_type_journal_request_t{
    uint32_t request;
    uint64_t event_id;   //delete events till id], or get event #id
} __attribute__((packed));




// indi for iec60870  ----------------------------------------------

#define IEC60870_TYPE_FLOAT  0
#define IEC60870_TYPE_INT    1
#define IEC60870_TYPE_BOOL   2

struct iec60870_data_t{
    uint32_t type;   //IEC60870_TYPE_FLOAT, IEC60870_TYPE_INT, IEC60870_TYPE_BOOL
    union data{
        float    f;
        int32_t  i;
        uint32_t b;
    };
    uint32_t address;
} __attribute__((packed));


struct msg_iec60870_data_t{
   uint8_t       md5_hash[16];
   surza_time_t  time;
   uint32_t      NOfData;
   unsigned      data_offset;   //data offset (msg_iec60870_data_t -> iec60870_data_t[0])
   // iec60870_data_t[NOfData];
} __attribute__((packed));

//-------------------------------------------------------------------


// set surza input data

#define SURZA_INPUT_TYPE_FLOAT    0
#define SURZA_INPUT_TYPE_INT32    1
#define SURZA_INPUT_TYPE_BOOL     2

struct input_value_t {
    uint32_t type;   // SURZA_INPUT_TYPE
    uint32_t index;  // input index
    union {
        float    f;
        int32_t  i;
        uint32_t b;
    } val;
} __attribute__((packed));

struct msg_type_set_input_t {
    uint8_t  hash[16]; //config id
    uint32_t num;
    uint8_t  reserv[16];
    //input_value_t values[num];
} __attribute__((packed));

//----------------------------------




// surza commands ------------------------

struct msg_type_command_t {
    uint8_t  hash[16]; //config id
    uint32_t index;    //BOOL_IN index
    uint8_t  reserv[16];
} __attribute__((packed));

//----------------------------------






// command messages for gate

#define GATE_COMMAND_DB_REDUCE_CONFIGS  1   //удаление всех событий и соответствующих настроек из базы до момента последнего изменения конфига
#define GATE_COMMAND_CONFIG_REQUEST     2   //запрос файла конфигурации от шлюза (в данных  - 16 байт хеша запрашиваемого конфига)

struct msg_type_gate_command_t{
    uint32_t command;
    uint32_t command_data_size;
    // uint8_t command_data[command_data_size];  //опционально дополнительные данные для команд
} __attribute__((packed));






//gate settings

#define GATE_SETTING_IP_A   100
#define GATE_SETTING_IP_B   101
#define GATE_SETTING_IP_C   102

#define GATE_SETTING_NTP_1  200
#define GATE_SETTING_NTP_2  201

//структура для настройки сетевых интерфейсов
struct gate_setting_ip{
    uint8_t ip[4];
    uint8_t mask[4];
} __attribute__((packed));

// ip адреса ntp северов
struct gate_setting_ntp{
    uint8_t ip[4];
}  __attribute__((packed));


struct gate_setting_t{
    uint32_t size;    //полный размер структуры, включая размер данных настройки и учитывая начальное смещение до нее
    uint32_t type;    //тип настройки
    uint32_t payload_size;   //размер данных настройки (для )
    uint32_t payload_offset; //смещение от начала структуры до данных настройки
    //payload[payload_size]
} __attribute__((packed));

struct msg_type_gate_settings_t{
    uint32_t n_of_settings;  //количество настроек добавленных в структуру
    uint32_t is_request;     //запрос или считывание
    uint32_t payload_size;   //общая длина настроек в структуре
    uint32_t payload_offset; //смещение от начала структуры до первой настройки
    //sequence of gate_setting_t
} __attribute__((packed));







#pragma pack()


#endif // NET_MESSAGES_H
