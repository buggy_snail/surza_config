#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QByteArray>
#include <QTabWidget>

#include <map>
#include <utility>
#include <vector>
#include <chrono>
#include <cstdint>

#include "common.h"
#include "net_connect.h"
#include "net_task_common_t.h"
#include "net_task_download_settings_t.h"
#include "param_tree.h"
#include "tabgatesettings.h"
#include "tabsurzasettings.h"

#define SETTINGS_REQUEST_RETRY_MSEC  3000


class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:
    void ip_adr_changed(QString ip, QString port);
    void RequestNewSettings();
    void WrongParams();
    void NewParams(const ParamTreeItem*);
    void NewIndi(const indi_type_t*);

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:

    //вкладки
    QTabWidget* main_tab;

    TabGateSettings*  tab_gate_settings;
    TabSurzaSettings* tab_surza_settings;


    typedef std::map<QString, QString> settings_t;
    settings_t settings;
    const settings_t default_settings={
        {"surza_ip","192.168.0.1"},
        {"surza_port","10030"}};
    const QString settings_filename = "surza_config.cfg";
    void settings_load(settings_t&);
    void settings_save(const settings_t&);
    bool GetKeyValuePair(const QString& str, std::pair<QString, QString>& key_value);


    //виджеты и переменные фрейма связи с сурзой
    const QString connection_string[2] = {
        "Состояние связи с СУРЗА:  Нет связи!    ",
        "Состояние связи с СУРЗА:  Связь установлена    "};
    void GateReconnect();

    NetConnect* con_gate;

    void connection_recv();
    void connection_results(__attribute__((unused)) uint32_t label, __attribute__((unused)) bool res){}

    std::vector<net_task_base_t*> msg_receivers;

    net_task_download_settings_t* net_task_download_settings;
    net_task_common_t* net_task_common;
    //--------------------------------------------


    //полученная конфигурация
    QByteArray settings_hash;
    std::chrono::time_point<std::chrono::steady_clock> settings_request_timestamp;
    void NewSettings(const QByteArray& hash, std::vector<QString>& strings);
    ParamTreeItem ParamTree;


    //все индикаторы по типам
    indi_type_t indi;
    //получено новое сообщение с индикаторами
    void ParseNewIndi(QByteArray);
    //создание нового массива индикаторов на основе новых настроек
    void SetupIndi(ParamTreeItem&);


};

#endif // MAINWINDOW_H
