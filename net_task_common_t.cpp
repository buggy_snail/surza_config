#include "net_task_common_t.h"

#include <utility>
#include <cstring>
#include <new>

net_task_common_t::net_task_common_t(QObject *parent, NetConnect** con) : net_task_base_t(parent, con){

    Subscribe(GateMessageType::SURZA_INDI);
    Subscribe(GateMessageType::GATE_SETTINGS);

}


bool net_task_common_t::get_new_indi(uint8_t& client, QByteArray& arr)
{
   if(indi_arr.isEmpty())
       return false;

   client = indi_client;
   arr = std::move(indi_arr);

   return true;
}


void net_task_common_t::set_param(QByteArray hash, uint16_t num, surza_value_t value)
{
    if(value.type>2)
        return;

    log("Отправка команды изменения параметра");

    msg_type_set_param_t p;

    std::memcpy(p.hash, hash.data(), 16);
    p.num = num;
    p.value.i32 = value.value.i32;


    if(!Send({static_cast<uint8_t>(GateMessageType::SURZA_SET_PARAM), 0, GATE_MSG_PRIORITY::MEDIUM, static_cast<unsigned>(sizeof(msg_type_set_param_t)), reinterpret_cast<uint8_t*>(&p)})){
        log("Ошибка при попытке отправки команды изменения параметра!");
    }
}

void net_task_common_t::set_all_params_default(QByteArray hash)
{
    log("Отправка команды установки всех параметров в значения по умолчанию");

    msg_type_set_param_t p;

    std::memcpy(p.hash, hash.data(), 16);
    p.num = 0xffff;

    if(!Send({static_cast<uint8_t>(GateMessageType::SURZA_SET_PARAM), 0, GATE_MSG_PRIORITY::MEDIUM, static_cast<unsigned>(sizeof(msg_type_set_param_t)), reinterpret_cast<uint8_t*>(&p)})){
        log("Ошибка при попытке отправки команды изменения параметра!");
    }

}

void net_task_common_t::set_setpoint(QByteArray hash, unsigned num, surza_value_t value)
{
    if(value.type>2)
        return;

    log("Отправка уставки");


    std::unique_ptr<uint8_t[]> buf;

    unsigned size = sizeof(msg_type_set_input_t)+sizeof(input_value_t);

    try {
        buf.reset(new uint8_t[size]);
    } catch (std::bad_alloc){
        log("Ошибка выделения памяти под сообщение!");
        return;
    }

    msg_type_set_input_t* msg_data = reinterpret_cast<msg_type_set_input_t*>(buf.get());
    input_value_t* data_ptr = reinterpret_cast<input_value_t*>(msg_data+1);

    std::memcpy(msg_data->hash, hash.data(), 16);
    msg_data->num = 1;

    data_ptr->type = value.type;
    data_ptr->index = num;
    data_ptr->val.i = value.value.i32;

    if(!Send({static_cast<uint8_t>(GateMessageType::SURZA_SET_INPUT), 0, GATE_MSG_PRIORITY::MEDIUM, size, reinterpret_cast<uint8_t*>(msg_data)}))
        log("Ошибка при попытке отправки уставки!");

}


void net_task_common_t::send_command(QByteArray hash, unsigned num)
{      
    log("Отправка команды");

    msg_type_command_t msg;

    std::memcpy(msg.hash, hash.data(), 16);
    msg.index = num;

    if(!Send({static_cast<uint8_t>(GateMessageType::SURZA_COMMAND), 0, GATE_MSG_PRIORITY::MEDIUM, sizeof(msg_type_command_t), reinterpret_cast<uint8_t*>(&msg)}))
        log("Ошибка при попытке отправки команды!");

}

void net_task_common_t::send_gate_settings(QByteArray data)
{
    log("Отправка настроек шлюза");

    if(!Send({static_cast<uint8_t>(GateMessageType::GATE_SETTINGS), 0, GATE_MSG_PRIORITY::MEDIUM, static_cast<unsigned>(data.size()), reinterpret_cast<uint8_t*>(data.data())}))
        log("Ошибка при попытке отправки настроек шлюза!");
}

void net_task_common_t::send_gate_command(unsigned command, QByteArray data)
{
    log("Отправка команды шлюза");

    unsigned size = sizeof(msg_type_gate_command_t)+static_cast<unsigned>(data.size());
    msg_type_gate_command_t* msg = reinterpret_cast<msg_type_gate_command_t*>(new (std::nothrow) uint8_t[size]);
    if(!msg)
        return;

    msg->command = command;
    msg->command_data_size = static_cast<unsigned>(data.size());
    if(msg->command_data_size)
        std::memcpy(msg+1, data.data(), msg->command_data_size);

    if(!Send({static_cast<uint8_t>(GateMessageType::GATE_COMMAND), 0, GATE_MSG_PRIORITY::MEDIUM, size, reinterpret_cast<uint8_t*>(&msg)}))
        log("Ошибка при попытке отправки команды шлюза!");

    delete[] msg;
}

void net_task_common_t::send_result(__attribute__((unused)) uint32_t label, __attribute__((unused)) bool result)
{

}


void net_task_common_t::new_msg(const net_connect_msg_t &msg)
{

    switch(static_cast<GateMessageType>(msg.Type())){
    case GateMessageType::SURZA_INDI:
            indi_arr.clear();
            indi_arr.append(reinterpret_cast<char*>(msg.Data()), static_cast<int>(msg.DataSize()));
            indi_client = msg.Client();
            emit new_indi();
        break;
    case GateMessageType::GATE_SETTINGS:
            new_gate_settings_msg(msg);
        break;

    default: break;
    }

    return;

}

void net_task_common_t::new_gate_settings_msg(const net_connect_msg_t& msg)
{
    if(msg.DataSize()<sizeof(msg_type_gate_settings_t))
        return;

    QByteArray arr;
    arr.append(reinterpret_cast<char*>(msg.Data()), static_cast<int>(msg.DataSize()));

    emit new_gate_settings(arr);
}

