#include "tabsurzasettings.h"

#include <utility>

#include <QVBoxLayout>
#include <QHeaderView>
#include <QList>

#include "dialogparamedit.h"

TabSurzaSettings::TabSurzaSettings(QWidget *parent, bool full_access) : QWidget(parent)
{
    this->full_access = full_access;

    QVBoxLayout* layout = new QVBoxLayout(this);

    params_table  = new QTableWidget(0, full_access?9:5, this);

    params_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    params_table->horizontalHeader()->setStretchLastSection(true);
    params_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    params_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    params_table->setSelectionMode(QAbstractItemView::SingleSelection);

    if(full_access){
        params_table->setHorizontalHeaderItem(0, new QTableWidgetItem("Тип"));
        params_table->setHorizontalHeaderItem(1, new QTableWidgetItem("№ входа"));
        params_table->setHorizontalHeaderItem(2, new QTableWidgetItem("Описание входа"));
        params_table->setHorizontalHeaderItem(3, new QTableWidgetItem("Минимум"));
        params_table->setHorizontalHeaderItem(4, new QTableWidgetItem("Максимум"));
        params_table->setHorizontalHeaderItem(5, new QTableWidgetItem("По-умолчанию"));
        params_table->setHorizontalHeaderItem(6, new QTableWidgetItem("Текущее"));
        params_table->setHorizontalHeaderItem(7, new QTableWidgetItem("Доступность"));
        params_table->setHorizontalHeaderItem(8, new QTableWidgetItem("Описание"));
    }else{
        params_table->setHorizontalHeaderItem(0, new QTableWidgetItem("Название"));
        params_table->setHorizontalHeaderItem(1, new QTableWidgetItem("Минимум"));
        params_table->setHorizontalHeaderItem(2, new QTableWidgetItem("Максимум"));
        params_table->setHorizontalHeaderItem(3, new QTableWidgetItem("Текущее значение"));
        params_table->setHorizontalHeaderItem(4, new QTableWidgetItem("Описание"));
    }

    layout->addWidget(params_table);

    this->setLayout(layout);

    connect(params_table, &QTableWidget::doubleClicked, this, &TabSurzaSettings::Edit);

}

void TabSurzaSettings::SetNewConfig(const ParamTreeItem *p){
    ClearWidgets();
    params.clear();
    if(!SetConfig(p))
        params.clear();
    else
        SetupWidgets();
}

void TabSurzaSettings::ClearWidgets()
{
    params_table->clearContents();
}

void TabSurzaSettings::SetupWidgets()
{
    //заполнение виджета таблицы
    params_table->setUpdatesEnabled(false);
    params_table->clearContents();
    params_table->setRowCount(static_cast<int>(params.size()));
    int row=0;
    for(auto& t : params){
        if(full_access){
            params_table->setItem(row, 0, new  QTableWidgetItem(surza_value_t::TYPE_NAMES[t.type]));
            params_table->setItem(row, 1, new  QTableWidgetItem(QString::number(t.index+1)));
            params_table->setItem(row, 2, new  QTableWidgetItem(""));
            params_table->setItem(row, 3, new  QTableWidgetItem(t.value_min_str));
            params_table->setItem(row, 4, new  QTableWidgetItem(t.value_max_str));
            params_table->setItem(row, 5, new  QTableWidgetItem(t.value_default_str));
            params_table->setItem(row, 6, new  QTableWidgetItem(""));
            params_table->setItem(row, 7, new  QTableWidgetItem(t.visible?QString(QChar(0x2705)):""));
            params_table->setItem(row, 8, new  QTableWidgetItem(t.description));
        }else{
            params_table->setItem(row, 0, new  QTableWidgetItem(""));
            params_table->setItem(row, 1, new  QTableWidgetItem(t.value_min_str));
            params_table->setItem(row, 2, new  QTableWidgetItem(t.value_max_str));
            params_table->setItem(row, 3, new  QTableWidgetItem(""));
            params_table->setItem(row, 4, new  QTableWidgetItem(t.description));
        }

        row++;
    }
    params_table->setUpdatesEnabled(true);
    params_table->update();
}

void TabSurzaSettings::Update(const indi_type_t *indi)
{
    //обновление полей виджета таблицы
    params_table->setUpdatesEnabled(false);
    int row=-1;
    for(auto& t : params){

        row++;

        unsigned type=0;
        switch(t.type){
        case surza_value_t::TYPE::FLOAT: type=SURZA_TABLE_TYPE::TABLE_IN_REAL; break;
        case surza_value_t::TYPE::INT32: type=SURZA_TABLE_TYPE::TABLE_IN_INT; break;
        case surza_value_t::TYPE::BOOL: type=SURZA_TABLE_TYPE::TABLE_IN_BOOL; break;
        }
        if(indi->size()<=type || indi->at(type).size()<=t.index)
            continue;

        auto ind = indi->at(type)[t.index];

        params_table->item(row, full_access?2:0)->setText(ind.description);
        params_table->item(row, full_access?6:3)->setText(ind.value.print());

        t.current_value = ind.value;
    }
    params_table->setUpdatesEnabled(true);
    params_table->update();
}


bool TabSurzaSettings::SetConfig(const ParamTreeItem *conf)
{

    //поиск узла со строками
    ParamTreeItem* strings_node = conf->FindNode("STRINGS");
    if(!strings_node)
        return false;

    //поиск узла параметров
    ParamTreeItem* params_node = conf->FindNode("PARAMS");
    if(!params_node)
        return false;

    //считывание всех параметров
    QString str;
    bool ok;
    unsigned u32;
    unsigned cnt=0;

    ParamTreeItem* rec_node = params_node->FindNode(QString::number(cnt));


    while(rec_node){

        if(!rec_node->GetItemValue("type", str)) return false;
        u32 = str.toUInt(&ok);
        if(!ok || ( u32!=TABLE_IN_REAL
                    && u32!=TABLE_IN_INT
                    && u32!=TABLE_IN_BOOL )) return false;

        params_t t(static_cast<SURZA_TABLE_TYPE>(u32));

        if(!rec_node->GetItemValue("num", str)) return false;
        t.index = str.toUInt(&ok);
        if(!ok) return false;

        if(!rec_node->GetItemValue("min", t.value_min_str)) return false;
        if(!rec_node->GetItemValue("max", t.value_max_str)) return false;
        if(!rec_node->GetItemValue("default", t.value_default_str)) return false;

        t.min.Set(t.value_min_str);
        t.max.Set(t.value_max_str);

        if(!rec_node->GetItemValue("visible", str)) return false;
        u32 = str.toUInt(&ok);
        if(!ok || u32>1) return false;
        t.visible = u32?true:false;

        if(!rec_node->GetItemValue("description", str)) return false;
        if(!strings_node->GetItemValue(str, t.description)) return false;

        t.param_num = static_cast<uint16_t>(cnt);

        if(full_access || t.visible)
            params.emplace_back(std::move(t));

        cnt++;
        rec_node = params_node->FindNode(QString::number(cnt));
    }

    return  true;
}

void TabSurzaSettings::Edit()
{
    int n = GetSelectedRow();
    if(n<0)
        return;

    unsigned index = static_cast<unsigned>(n);

    DialogParamEdit dialog(this,  params_table->item(n, full_access?2:0)->text(), params[index].value_min_str, params[index].value_max_str, params[index].current_value);
    dialog.exec();
    if(!dialog.Status())
        return;

    //проверка значения
    auto new_value = dialog.GetNewValue();
    if(new_value>params[index].max || new_value<params[index].min){
        show_msg_box("Новое значение параметра вне допустимого диапазона!");
        return;
    }

    emit SetParam(params[index].param_num, new_value);
}


int TabSurzaSettings::GetSelectedRow()
{
    QList<QTableWidgetItem*>items = params_table->selectedItems();
    if(!items.size())
        return -1;

    return params_table->row(items[0]);
}

unsigned TabSurzaSettings::RowToIndex(int row)
{
    if(full_access)
        return static_cast<unsigned>(row);

     //с учетом невидимых в таблице
    unsigned i;
    for(i=0; i<params.size(); i++){
        if(params[i].visible)
            row--;
        if(row<0)
            break;
    }

    return i;
}
