#include "getipstringdialog.h"

#include <QVBoxLayout>
#include <QLabel>

GetIPStringDialog::GetIPStringDialog(QWidget *parent, const QString& ip_str, const QString& port_str) : QDialog(parent)
{
    ok_button = false;

    setModal(true);
    setWindowTitle("Введите IP адрес");

    QVBoxLayout* layout = new QVBoxLayout(this);
    this->setLayout(layout);

    layout->addWidget(new QLabel("IP адрес:"));

    edit_ip = new QLineEdit;
    edit_ip->setText(ip_str);
    layout->addWidget(edit_ip);

    layout->addWidget(new QLabel("Порт:"));

    edit_port = new QLineEdit;
    edit_port->setText(port_str);
    layout->addWidget(edit_port);

    button = new QPushButton(this);
    button->setText("Ok");
    layout->addWidget(button);
    connect(button, &QPushButton::clicked, [this](){
        str_ip = edit_ip->text();
        str_port = edit_port->text();
        ok_button = true;
        this->close();
    });

    this->setFixedSize(layout->minimumSize());
}



