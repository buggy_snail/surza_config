#ifndef CRC32_H
#define CRC32_H


#include "stdint.h"

uint32_t crc32(const char* buf, unsigned len);

bool crc32_check(const char* buf, unsigned len, uint32_t crc32);


#endif // CRC32_H
