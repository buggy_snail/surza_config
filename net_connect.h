#ifndef NET_CONNECT_H
#define NET_CONNECT_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>
#include <QTimer>

#include <utility>
#include <deque>
#include <set>
#include <mutex>
#include <memory>
#include <vector>

#include <stdint.h>
#include <chrono>

#include "net_messages.h"
#include "net_connect_msg.h"



#define  DEFAULT_GATE_PORT   10030



#define  NET_CONNECT_SEND_QUEUE_LENGTH_MAX         1000

#define  NET_CONNECT_MSG_SEND_TIMEOUT_MSEC         6000
#define  NET_CONNECT_SPEC_MSG_SEND_TIMEOUT_MSEC    3000

#define  NET_CONNECT_RECV_QUEUE_MAX_LENGTH         500

#define  NET_CONNECT_RECONNECT_DELAY_MSEC          1500





class net_connect_sock : public QObject{
    Q_OBJECT
public:
    explicit net_connect_sock(QTcpSocket* sock, QHostAddress adr, quint16 port, QObject *parent = nullptr);

    bool IsConnected();   // проверка наличия соединения

public slots:

    void SockError(QAbstractSocket::SocketError); // ошибка сокета
    void Disconnected();                          // потеря соединения
    void DropConnection();                        // необходимость разрыва соединения
    void ReconnectTimeout();                      // сработал таймер переподключения

private:
    QTcpSocket* sock;
    QHostAddress adr;
    quint16 port;
    QTimer* reconnect_timer;
    void reconnect();
};


class net_connect_sender : public QObject{
    Q_OBJECT
public:
    explicit net_connect_sender(QTcpSocket* sock, QObject *parent = nullptr);
    ~net_connect_sender();

    bool AddMsg(net_connect_msg_t&&);

signals:

    void MsgSendResult(uint32_t label, bool res);   // результат отправки сообщения (отправилось \ не получилось отправить до истечения таймаута)
    void DropConnection();      // сигнал о необходимости разрыва соединения
    void MsgAdded();            // новое сообщение добавлено

public slots:
    void Connected();            // соединение установлено
    void Disconnected();         // соединенение разорвано
    void DataSent(qint64 bytes); // данные отправлены

private slots:
    void SendMsg();              // отправка сообщения
    void TimeoutsCheck();        // проверка таймаутов у сообщений в очереди на отправку
    void MsgTimeout();           // срабатывание таймера на сообщение
    void SpecialMsgTimeout();    // срабатывание таймера на специальное сообщение

private:

    QTcpSocket* sock;

    static const uint8_t priority_num = static_cast<uint8_t>(GATE_MSG_PRIORITY::BACKGROUND)+1;
    std::deque<net_connect_msg_t> queue[priority_num];
    std::deque<net_connect_msg_t>::size_type queue_cnt();
    std::deque<net_connect_msg_t> special_queue;
    std::mutex queue_mutex;
    void clear_queue();
    void clear_special_queue();

    QTimer* msg_timer;
    QTimer* spec_msg_timer;
    QTimer* check_timeouts_timer;
    /*
    void msg_timer_start(int msec);
    void spec_msg_timer_start(int msec);
    */

    enum write_states{
      WR_DISCONNECT=0,
      WR_NO_MSG,
      WR_PROCESS
    } state;


    std::vector<net_connect_msg_t> cur_msg;
    std::vector<net_connect_msg_t> cur_spec_msg;
    uint32_t byte_counter;
    qint64 bytes_sent;
    uint8_t* data_ptr;

    net_connect_msg_t tmp_msg;

    void write();

    bool pop_from_queue(net_connect_msg_t&);
    bool pop_from_special_queue(net_connect_msg_t&);

};


class net_connect_receiver : public QObject{
    Q_OBJECT
public:
    explicit net_connect_receiver(QTcpSocket* sock, QObject *parent = nullptr);
    ~net_connect_receiver();

    bool GetMessage(net_connect_msg_t&);
    bool GetSpecialMessage(net_connect_msg_t&);

signals:

    void NewMsgReceived();  //сигнал о добавлении нового сообщения в очередь приема
    void NewMsgToSend();    //сигнал о добавлении нового сообщения на передачу (в служебную очередь)
    void DropConnection();  //сигнал о необходимости разрыва соединения

public slots:
    void Disconnected();    // соединенение разорвано
    void DataAvailable();   // доступность новых данных

private slots:
    void MsgTimeout();      // срабатывание таймера ожидания сборки всего сообщения

private:

    enum read_states{
      RD_INITIAL=0,
      RD_NO_MSG,
      RD_READ_MSG,
      RD_WAIT_DISCONNECT
    } state;

    QTcpSocket* sock;

    static const uint8_t priority_num = static_cast<uint8_t>(GATE_MSG_PRIORITY::BACKGROUND)+1;

    std::deque<net_connect_msg_t> recv_queue[priority_num];
    std::deque<net_connect_msg_t> send_queue;

    std::mutex recv_queue_mutex;

    QTimer* msg_timer;

    qint64 bytes_left;
    char* data_ptr;
    net_raw_msg_t* msg_ptr;

    net_connect_msg_t msg;


    //обработка принятого сообщения, возвращает false если сообщение не корректное и необходимо разорвать соединение
    bool process_msg();

};





class NetConnect : public QObject
{
    Q_OBJECT
public:
    explicit NetConnect(QObject *parent = nullptr, QHostAddress adr = QHostAddress(QHostAddress::LocalHost), quint16 port = DEFAULT_GATE_PORT);
    ~NetConnect();

    //mark channel as low priority
    void SetLowPriority(bool);

    //subscribe for specific message types
    void Subscribe(GateMessageType type);
    void Unsubscribe(GateMessageType type);

    //send message
    bool SendMsg(net_connect_msg_t&&);
    bool SendMsg(const net_connect_msg_t&);

    //recv message
    bool RecvMsg(net_connect_msg_t&);

    //update module watchdog
    void Update(void);

    //connection check
    bool IsConnected(void){return sock_handler->IsConnected();}

signals:

    void MsgReceived();       //сигнал о добавлении нового сообщения в очередь приема
    void MsgSendResult(uint32_t label, bool result); // результат отправки сообщения (отправилось \ не получилось отправить до истечения таймаута)

public slots:


private:

    QHostAddress adr;
    quint16 port;

    QTcpSocket sock;


    net_connect_sock* sock_handler;
    net_connect_receiver* receiver;
    net_connect_sender* sender;

    bool low_priority_channel;

    uint8_t machine_id[16];
    int32_t pid;

    std::set<GateMessageType> subscribe_types;

    void GetMachineID();


private slots:

    void NewSpecialMsgNotification();   //уведомление о наличии специальных сообщений от приемника на передачу
    void NewConnectionNotification();   //уведомление о новом соединении

};


#endif // NET_CONNECT_H
