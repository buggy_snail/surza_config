#ifndef PARAM_TREE_H
#define PARAM_TREE_H

#include <string>
#include <deque>
#include <vector>
#include <utility>
#include <type_traits>

#include <QString>


class ParamTreeItem{

public:

    ParamTreeItem(bool is_node=true, const QString& name="Unknown_parameter", const QString& value=""){node=is_node; this->name=name; this->value=value;}
    ~ParamTreeItem(){Clear();}

    void Clear(){for(auto item : items) delete item; items.clear();}

    ParamTreeItem* AddNode(const QString& name){ParamTreeItem* node = new ParamTreeItem(true, name); items.push_back(node); return node;}
    void AddItem(const QString& name, const QString& value){items.push_back(new ParamTreeItem(false, name, value));}

    bool GetItemValue(const QString& item_name, QString& value) const;
    bool GetItemValue(unsigned item_num, QString& value) const;
    bool GetItemNameValue(unsigned item_num, QString& name, QString &value) const;
    void GetItems(std::vector<std::pair<QString, QString>>&) const;
    void GetItems(std::vector<QString>&) const;

    unsigned ItemNum() const;
    ParamTreeItem* FindNode(const QString& node_name) const;

    template<class Iterator>
    typename std::enable_if<std::is_same<typename Iterator::value_type, QString>::value, bool>::type
    AddStrings(Iterator first, Iterator end){
        Iterator it = first;

        while(it!=end){

            if(!is_node(*it)){

               if(!add_item(*it))
                   return false;
               it++;

            }else{

                QString pair = get_pair(*it);

                it++;
                Iterator node_first=it;

                bool found=false;
                while(it!=end && !found){
                    if(*it==pair){
                        found=true;
                        break;
                    }
                    it++;
                }

                if(!found)
                    return false;

                Iterator node_end=it;
                it++;

                ParamTreeItem* node = add_node(*node_end);
                if(!node)
                    return false;

                if(!node->AddStrings(node_first, node_end))
                    return false;

            }

        }

        return true;
    }


    static QString spacer;
    static QString node_prefix;

    bool IsNode() const {return node;}

    QString String() const {return node?(node_prefix+name):(name+"="+value);}
    void GetStrings(std::vector<QString>&, bool add_spacer=true) const;

private:

    bool node;
    QString name;
    QString value;

    std::deque<ParamTreeItem*> items;

    bool add_item(QString);
    ParamTreeItem* add_node(QString);

    void remove_spacer_and_prefix(QString&);

    bool is_node(const QString&) const;
    QString get_pair(const QString&) const;

};




#endif // PARAM_TREE_H
