#ifndef NET_TASK_DOWNLOAD_SETTINGS_T_H
#define NET_TASK_DOWNLOAD_SETTINGS_T_H

#include "net_task_base_t.h"

#include <cstdint>
#include <vector>

#include <QString>
#include <QTimer>
#include <QByteArray>


class net_task_download_settings_t : public net_task_base_t
{
    Q_OBJECT
public:
    net_task_download_settings_t(QObject *parent = nullptr, NetConnect** con=nullptr);

    bool get_new_settings(QByteArray& hash, std::vector<QString>&);

signals:
    void new_settings(void);

public slots:

    void download();

private:
        void send_result(uint32_t label, bool result);
        void new_msg(const net_connect_msg_t& msg);

        QTimer* timer;
        bool wait_response_flag;

        QByteArray data;
        QByteArray hash;
};

#endif // NET_TASK_DOWNLOAD_SETTINGS_T_H
