#-------------------------------------------------
#
# Project created by QtCreator 2019-10-25T13:37:41
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = surza_config
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        common.cpp \
        crc32.cpp \
        dialogparamedit.cpp \
        getipstringdialog.cpp \
        main.cpp \
        mainwindow.cpp \
        net_connect.cpp \
        net_connect_msg.cpp \
        net_task_common_t.cpp \
        net_task_download_settings_t.cpp \
        param_tree.cpp \
        tabgatesettings.cpp \
        tabsurzasettings.cpp

HEADERS += \
        common.h \
        crc32.h \
        dialogparamedit.h \
        getipstringdialog.h \
        mainwindow.h \
        net_connect.h \
        net_connect_msg.h \
        net_messages.h \
        net_task_base_t.h \
        net_task_common_t.h \
        net_task_download_settings_t.h \
        param_tree.h \
        tabgatesettings.h \
        tabsurzasettings.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
