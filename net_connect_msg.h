#ifndef NET_CONNECT_MSG_H
#define NET_CONNECT_MSG_H


#include <stdint.h>
#include <atomic>
#include <chrono>

#include "net_messages.h"



#define  NET_CONNECT_MSG_DATA_LENGTH_MAX    (32*1024*1024)


class net_connect_msg_t{

public:

    //может вызвать  std::length_error, std::bad_alloc
    net_connect_msg_t(uint8_t type=static_cast<uint8_t>(GateMessageType::GATE_SPECIAL_MSG_TYPE_MAX_NUM), uint8_t client_num=0, GATE_MSG_PRIORITY priority=GATE_MSG_PRIORITY::BACKGROUND, unsigned data_size=0, const uint8_t* data_ptr=nullptr);

    net_connect_msg_t(const net_connect_msg_t&);
    net_connect_msg_t(net_connect_msg_t&&);

    ~net_connect_msg_t();

    net_connect_msg_t& operator=(const net_connect_msg_t&);  //может вызвать std::bad_alloc
    net_connect_msg_t& operator=(net_connect_msg_t&&);

    static uint32_t GetDataSizeFromMsgSize(uint32_t size);      //получение длины данных из общей длины сообщения
    static uint32_t GetMsgSizeFromDataSize(uint32_t data_size); //получение общей длины сообщения из длины данных

    void SetType(uint8_t type);
    void SetClient(uint8_t client_num);
    void SetPriority(GATE_MSG_PRIORITY priority);

    uint32_t MakeLabel();   //сгенерировать новую уникальную метку сообщения
    uint32_t Label() const;

    void SetTimestamp();    //проставить временную метку сообщения текущим временем
    const std::chrono::time_point<std::chrono::steady_clock>& Timestamp() const {return timestamp;}  //временная метка сообщения

    uint8_t Type() const;
    uint8_t Client() const;
    GATE_MSG_PRIORITY Priority() const;
    uint32_t DataSize() const;

    uint32_t MsgSize() const {return raw_data_size;}

    //сброс и освобождение ресурсов и выделение памяти под новое сообщение с длиной полезных данных data_size
    bool Reset(uint32_t data_size);

    bool CheckMsg() const;     //проверка сообщения на корректность

    uint8_t* Msg() const {return raw_data_ptr;}   //указатель на сообщение
    uint8_t* Data() const;                        //указатель на данные


private:

    uint32_t raw_data_size;
    uint8_t*  raw_data_ptr;

    std::chrono::time_point<std::chrono::steady_clock> timestamp;

    static std::atomic<uint32_t> label_global;

    void SetMsgSize(uint32_t msg_size);    //не выделяет память, просто присваивает значению полю данных
    void SetDataSize(uint32_t data_size);  //не выделяет память, просто присваивает значению полю данных
    void SetLabel(uint32_t label);

    void alloc_memory(uint32_t);
    void dealloc_memory();

    void move(net_connect_msg_t* dst, net_connect_msg_t* src);

};




#endif // NET_CONNECT_MSG_H
