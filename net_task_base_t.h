#ifndef NET_TASK_BASE_T_H
#define NET_TASK_BASE_T_H

#include <QObject>
#include <QString>

#include <cstdint>
#include <set>
#include <stdexcept>

#include "net_connect.h"


class net_task_base_t : public QObject
{
    Q_OBJECT
public:
    explicit net_task_base_t(QObject *parent = nullptr, NetConnect** con=nullptr)  : QObject(parent) {
        if(!con)
            throw std::invalid_argument("Second argument can't be NULL!");

        this->con = con;
    }

    virtual void msg(const net_connect_msg_t& msg){
        if(msg_acceptable(msg.Type()))
                new_msg(msg);
    }

    void new_connection(){
        if(*con)
            for(auto type: subscribes)
                (*con)->Subscribe(static_cast<GateMessageType>(type));
    }


private:
    virtual void send_result(uint32_t label, bool result)=0;
    virtual void new_msg(const net_connect_msg_t& msg)=0;

    std::set<uint32_t> sent;
    std::set<uint8_t> subscribes;
    NetConnect** con;

protected:
    void Subscribe(GateMessageType type){subscribes.insert(static_cast<uint8_t>(type)); if(*con) (*con)->Subscribe(type);}
    void Unsubscribe(GateMessageType type){subscribes.erase(static_cast<uint8_t>(type)); if(*con) (*con)->Unsubscribe(type);}

    bool Send(net_connect_msg_t&& msg){
        if(*con==nullptr)
            return false;
        uint32_t label = msg.Label();
        sent.insert(label);
        bool ret = (*con)->SendMsg(std::move(msg));
        if(!ret)
            sent.erase(label);
        return ret;
    }

    bool Send(const net_connect_msg_t& msg){
        if(*con==nullptr)
            return false;
        uint32_t label = msg.Label();
        sent.insert(label);
        bool ret = (*con)->SendMsg(msg);
        if(!ret)
            sent.erase(label);
        return ret;
    }

    bool msg_acceptable(uint8_t type){
        auto search=subscribes.find(type);
        return (search != subscribes.end());
    }

    void log(const QString& text){emit log_message(text);}


signals:

    void log_message(QString);

public slots:

    void send_result_slot(uint32_t label, bool result){
        if(!sent.size())
            return;

        auto search=sent.find(label);
        if(search != sent.end()){
            sent.erase(label);
            send_result(label, result);
        }

    }

};

#endif // NET_TASK_BASE_T_H
