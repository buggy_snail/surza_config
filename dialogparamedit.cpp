#include "dialogparamedit.h"

#include <QLabel>
#include <QGridLayout>


DialogParamEdit::DialogParamEdit(QWidget *parent, QString name, QString Min, QString Max, const surza_value_t &value) : QDialog(parent)
{
    status = false;
    new_value = value;

    setModal(true);
    setWindowTitle("Изменить параметр");

    QGridLayout* layout = new QGridLayout(this);

    layout->addWidget(new QLabel(name), 0, 0, 1, 2);
    layout->addWidget(new QLabel("Минимальное значение:"), 1, 0, 1, 1);
    layout->addWidget(new QLabel(Min), 1, 1, 1, 1);
    layout->addWidget(new QLabel("Максимальное значение:"), 2, 0, 1, 1);
    layout->addWidget(new QLabel(Max), 2, 1, 1, 1);
    layout->addWidget(new QLabel("Текущее значение:"), 3, 0, 1, 1);
    layout->addWidget(new QLabel(value.print()), 3, 1, 1, 1);
    layout->addWidget(new QLabel("Новое значение:"), 4, 0, 1, 1);

    edit = new QLineEdit(this);
    edit->setAlignment(Qt::AlignCenter);
    edit->setText("");
    edit->setMinimumWidth(80);

    layout->addWidget(edit, 4, 1, 1, 1);

    button_cancel = new QPushButton(this);
    button_cancel->setText("Отмена");

    button_ok = new QPushButton(this);
    button_ok->setText("Изменить");
    button_ok->setDefault(true);


    layout->addWidget(button_ok, 5, 0, 1, 1);
    layout->addWidget(button_cancel, 5, 1, 1, 1);


    this->setLayout(layout);
    this->setFixedSize(layout->minimumSize());

    connect(this->button_cancel, &QPushButton::clicked, [this](){
            status = false;
            this->close();
        });

    connect(this->button_ok, &QPushButton::clicked, [this](){
        if(!edit->text().length()){
            show_msg_box("Новое значение параметра не задано!");
            return;
        }
        bool ok;
        switch(new_value.type){
        case surza_value_t::TYPE::FLOAT: new_value.set(edit->text().toFloat(&ok)); break;
        case surza_value_t::TYPE::INT32: new_value.set(edit->text().toInt(&ok)); break;
        case surza_value_t::TYPE::BOOL: {unsigned u = edit->text().toUInt(&ok);
                                         if(ok && u<2){new_value.set(u?true:false);}
                                         else ok=false;}
        }
        if(!ok){
            show_msg_box("Новое значение параметра задано некорректно!");
            return;
        }
        status = true;
        this->close();
        });

}
