#ifndef NET_TASK_COMMON_T_H
#define NET_TASK_COMMON_T_H

#include "net_task_base_t.h"

#include <QObject>
#include <QByteArray>

#include <list>

#include "common.h"


class net_task_common_t : public net_task_base_t
{
    Q_OBJECT
public:
    net_task_common_t(QObject *parent = nullptr, NetConnect** con=nullptr);

    bool get_new_indi(uint8_t& client, QByteArray&);

public slots:
    void set_param(QByteArray hash, uint16_t num, surza_value_t value);
    void set_all_params_default(QByteArray hash);
    void set_setpoint(QByteArray hash, unsigned num, surza_value_t value);
    void send_command(QByteArray hash, unsigned num);
    void send_gate_settings(QByteArray);
    void send_gate_command(unsigned, QByteArray);

signals:
    void new_indi(void);
    void new_journal_event(void);
    void new_gate_settings(QByteArray);

private:
    void send_result(uint32_t label, bool result);
    void new_msg(const net_connect_msg_t& msg);

    QByteArray indi_arr;
    uint8_t    indi_client;

    void new_gate_settings_msg(const net_connect_msg_t &msg);

};

#endif // NET_TASK_COMMON_T_H
