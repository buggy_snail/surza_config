
#include "common.h"

#include <QRegularExpression>


void show_msg_box(const QString &str, QMessageBox::Icon icon)
{
    QMessageBox msgBox;
    msgBox.setIcon(icon);
    msgBox.setText(str);
    msgBox.exec();
}




bool check_ip_string(const QString& str,  uint32_t* ip)
{
    if(!str.length())
        return false;

    QRegularExpression re("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    QRegularExpressionMatch match = re.match(str);
    if(!match.hasMatch())
        return false;


    if(ip){
        *ip = match.captured(1).toUInt();
        *ip*=256;
        *ip += match.captured(2).toUInt();
        *ip*=256;
        *ip += match.captured(3).toUInt();
        *ip*=256;
        *ip += match.captured(4).toUInt();
    }


    return true;
}

bool check_ip_string(const QString& str,  uint8_t ip[])
{
    if(!str.length())
        return false;

    QRegularExpression re("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    QRegularExpressionMatch match = re.match(str);
    if(!match.hasMatch())
        return false;

    for(int i=0; i<4; i++)
        ip[i] = static_cast<uint8_t>(match.captured(i+1).toUInt());

    return true;
}

bool check_mask_string(const QString& str,  uint8_t mask[])
{
    if(!str.length())
        return false;

    QRegularExpression re("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    QRegularExpressionMatch match = re.match(str);
    if(!match.hasMatch())
        return false;

    for(int i=0; i<4; i++)
        mask[i] = static_cast<uint8_t>(match.captured(i+1).toUInt());

    //проверка
    uint32_t ip32 = ((static_cast<uint32_t>(mask[0])<<24)
                    | (static_cast<uint32_t>(mask[1])<<16)
                    | (static_cast<uint32_t>(mask[2])<<8)
                    | static_cast<uint32_t>(mask[3]));

    if(!ip32)
        return false;

    while(ip32){
        if((ip32&0x80000000)==0 && (ip32&0x7fffffff)!=0)
            return false;
        ip32<<=1;
    }

    return true;
}






bool check_port_string(const QString& str,  uint16_t* port)
{
    if(!str.length())
        return false;

    bool ok;
    unsigned tmp = str.toUInt(&ok);
    if(!ok || tmp>0xffff)
        return false;

    if(port)
        *port=static_cast<uint16_t>(tmp);

    return true;
}


const QString indi_t::MAIN_TABLE_NAME[6] = {
      "IN_REAL",
      "IN_INT",
      "IN_BOOL",
      "OUT_REAL",
      "OUT_INT",
      "OUT_BOOL"
  };

const QString surza_value_t::TYPE_NAMES[3] = {
    "REAL",
    "INT",
    "BOOL"
};
